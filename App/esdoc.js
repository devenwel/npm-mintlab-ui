/**
 * ESDoc types with external documentation
 */

// React

/**
 * @external {ReactElement} https://github.com/facebook/react/blob/master/packages/react/src/ReactElement.js#L111
 */

/**
 * @external {React.Component} https://reactjs.org/docs/react-api.html#reactcomponent
 */

/**
 * @external {React.PureComponent} https://reactjs.org/docs/react-api.html#reactpurecomponent
 */

/**
 * @external {React.Fragment} https://reactjs.org/docs/react-api.html#reactfragment
 */

/**
 * @external {React.Children} https://reactjs.org/docs/react-api.html#reactchildren
 */

// DOM

/**
 * @external {Event} https://developer.mozilla.org/en-US/docs/Web/API/Event
 */

/**
 * @external {MutationObserver} https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver
 */
