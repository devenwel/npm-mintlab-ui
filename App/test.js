/**
 * Test utility functions.
 * ZS-TODO: publish on the npm registry and install as a dependency.
 */
import { renderToStaticMarkup } from 'react-dom/server';

/**
 * Replace the `document.body` content with a rendered React component.
 *
 * @example
 *   html(<Hello>World</Hello>);
 * @param {Object} component
 * @return {HTMLElement}
 */
export function html(component) {
  document.body.innerHTML = renderToStaticMarkup(component);

  return document.body.firstChild;
}
