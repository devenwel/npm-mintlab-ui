import React, { Component } from 'react';
import MuiSnackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import Icon from '../Icon/Icon';
import { bind, equals } from '../../library';

/**
 * Wrapper for @see https://material-ui-next.com/api/snackbar/
 *
 * @param {Object} props
 * @return {ReactElement}
 */
export default class Snackbar extends Component {

  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      open: false,
      autoHideDuration: 6000,
      key: new Date().getTime(),
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      currentMessage: {},
      queue: [props],
    };
    bind(this, 'handleClose', 'handleExited');
  }

  componentDidMount() {
    this.processQueue();
  }

  componentDidUpdate(prevProps) {
    if (!equals(this.props, prevProps)) {
      const {
        state: { queue, open },
      } = this;

      queue.push(this.props);
      this.setState({ queue });

      if (open) {
        this.setState({ open: false });
      } else {
        this.processQueue();
      }
    }
  }

  render() {
    const {
      state: {
        open,
        currentMessage,
      },
      handleClose,
      handleExited,
    } = this;

    return (
      <MuiSnackbar
        {...currentMessage}
        onClose={handleClose}
        onExited={handleExited}
        open={open}
        action={[
          <IconButton
            key="close"
            aria-label="Close"
            color="inherit"
            className="close"
            onClick={handleClose}
          >
            <Icon>close</Icon>
          </IconButton>,
        ]}
      />
    );
  }

  handleClose(event, reason) {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ open: false });
  }

  handleExited() {
    this.processQueue();
  }

  processQueue() {
    const { queue } = this.state;

    if (queue.length) {
      const currentMessage = queue.shift();

      this.setState({
        currentMessage,
        queue,
        open: true,
      });
    }
  }

}
