import React from 'react';
import Snackbar from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `Snackbar` component', () => {

  test('needs tests', () => {
    const actual = (
      <Snackbar/>
    );
    const expected = renderToStaticMarkup('');

    expect(actual).toBe(expected);
  });

});
