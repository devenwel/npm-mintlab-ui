# `Snackbar` component

>Wrapper component for https://material-ui-next.com/api/snackbar/

Shows a Snackbar with the supplied message. Pass in the configuration props (see the above documentation) to customize a snackbar. Only the `message` prop is required.

If a snackbar would be triggered while an existing snackbar is visible, the existing snackbar will animate out before the new snackbar is shown.

## example

    <Snackbar 
      message='This is a test message'
      anchorOrigin={
        vertical: 'bottom',
        horizontal: 'left',
      }
      autoHideDuration={4000}
    />

