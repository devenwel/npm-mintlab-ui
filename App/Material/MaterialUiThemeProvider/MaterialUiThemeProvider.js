import { createElement } from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import '../Roboto/roboto.css';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#1274EB',
      light: '#F0F4FF',
    },
    secondary: {
      main: '#bedf3a',
    },
    error: {
      main: '#AF0E27',
      light: '#FBE9E7',
    },
    grey: {
      defaultWeight: 100,
    },
  },
  zIndex: {
    inputLabel: 100,
    statusBar: 1250,
    options: 1260,
  },
  shape: {
    buttonBorderRadius: '20px',
  },
});

/**
 * Theme provider for wrapping the consumer's component tree root.
 *
 * @see https://material-ui-next.com/customization/themes/
 *
 * @example
 * <MaterialUiThemeProvider>
 *   <App/>
 * </MaterialUiThemeProvider>
 *
 * @param {Object} props
 * @param {*} props.children
 * @return {ReactElement}
 */
const MaterialUiThemeProvider = ({ children }) =>
  createElement(MuiThemeProvider, {
    children,
    theme,
  });

export default MaterialUiThemeProvider;
