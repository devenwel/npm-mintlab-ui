import React from 'react';
import Dialog from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `Dialog` component', () => {

  test('needs tests', () => {
    const actual = (
      <Dialog/>
    );
    const expected = renderToStaticMarkup('');

    expect(actual).toBe(expected);
  });

});
