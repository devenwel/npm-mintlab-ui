/** 
* @param {Object} theme
* @return {Object}
*/
const styles = () => ({
  root: {
    '& >*': {
      marginRight: '12px',
    },
  },
});

export default styles;
