import React from 'react';
import MuiDialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '../Button';
import { withStyles } from '@material-ui/core/styles';
import styles from './style/styles';

/**
 * @param {Object} props
 * @param {boolean} [props.open=false]
 * @param {Array} props.buttons
 * @param {string} props.text
 * @param {string} props.title
 * @return {ReactElement}
 */
const Dialog = ({
  open=false,
  buttons,
  text,
  title,
  classes,
}) => (
  <MuiDialog
    open={ open }
    aria-labelledby="modal-dialog-title"
    aria-describedby="modal-dialog-description"
  >

    <DialogTitle id="modal-dialog-title">{ title }</DialogTitle>
    <DialogContent>
      <DialogContentText id="modal-dialog-description">
        { text }
      </DialogContentText>
    </DialogContent>

    { buttons && buttons.length &&
      <DialogActions
        classes={ classes }
      >
        {
          buttons.map((button, i) => (
            <Button
              action={ button.onClick }
              presets={ button.presets }
              key={ i }
            >
              { button.text }
            </Button>
          ))
        }
      </DialogActions>
    }
  </MuiDialog>
);

export default withStyles(styles)(Dialog);
