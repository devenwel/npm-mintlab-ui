import React from 'react';
import MuiDrawer from '@material-ui/core/Drawer';

const Drawer = ({
  children,
  classes,
  onClose,
  open = true,
  variant = 'persistent',
}) => (
  <MuiDrawer
    classes={classes}
    onClose={onClose}
    open={open}
    variant={variant}
  >
    {children}
  </MuiDrawer>
);

export default Drawer;
