import React from 'react';
import Drawer from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `Drawer` component', () => {

  test('creates the expected HTML', () => {
    const actual = renderToStaticMarkup(<Drawer>TEST</Drawer>);
    const expected = '<div>TEST</div>';

    expect(actual).toBe(expected);
  });

});
