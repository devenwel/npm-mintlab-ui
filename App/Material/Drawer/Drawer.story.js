import {
  React,
  stories,
  text,
} from '../../story';
import Drawer from '.';

stories(module, __dirname, {
  Default() {
    return (
      <Drawer>
        {text('Greeting', 'Hello, world!')}
      </Drawer>
    );
  },
});
