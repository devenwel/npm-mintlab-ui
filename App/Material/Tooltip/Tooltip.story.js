import {
  React,
  stories,
  text,
  select,
} from '../../story';
import Tooltip from '.';

stories(module, __dirname, {
  Default() {
    return (
      <Tooltip 
        title={text('Title', 'Hello, world!')}
        placement={select('Placement', ['bottom-end', 'bottom-start', 'bottom', 'left-end', 'left-start', 'left', 'right-end', 'right-start', 'right', 'top-end', 'top-start', 'top'])}
        type={select('Type', ['default', 'error', 'info'])}
      >
        <div style={{
          border: '2px solid black',
          padding: '5px',
        }}>Tooltip</div>
      </Tooltip>
    );
  },
});
