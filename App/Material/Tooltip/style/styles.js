
/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the Tooltip component
 * 
 * @param {Object} theme
 */
const BORDER_RADIUS_NR = 2;

const styles = ({ 
  palette: {
    primary,
    common,
    error,
  },
  shape: {
    borderRadius,
  },
}) => ({
  all: {
    padding: '8px',
    maxWidth: '300px',
    borderRadius: borderRadius * BORDER_RADIUS_NR,
  },
  popper: {
    opacity: '1',
  },
  default: {
    backgroundColor: common.black,
  },
  error: {
    backgroundColor: error.dark,
  },
  info: {
    backgroundColor: primary.light,
    color: primary.main,
  },
});

export default styles;
