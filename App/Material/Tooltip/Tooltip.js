import React from 'react';
import MuiTooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/core/styles';
import styles from './style/styles';
import classNames from 'classnames';

/**
 * Wrapper component for Material UI Tooltip.
 * Additional props will be passed through to that component.
 * 
 * @see https://material-ui.com/api/tooltip/
 * 
 * @param {Object} props
 * @param {Array} props.children
 * @param {Object} props.classes
 * @param {string} [props.placement='top']
 * @param {string} [props.type='default']
 * @param {string} props.title
 * @return {ReactElement}
 */
const Tooltip = ({ 
  children,
  classes,
  title,
  placement = 'top',
  type = 'default',
  ...rest
}) => (
  <MuiTooltip
    title={title}
    placement={placement}
    classes={{
      tooltip: classNames(classes.all, classes[type]),
      popper: classes.popper,
    }}
    {...rest}
  >
    <div>{children}</div>
  </MuiTooltip>);

export default withStyles(styles)(Tooltip);
