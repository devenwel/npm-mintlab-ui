import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import MuiButton from '@material-ui/core/Button';
import Icon from '../Icon';
import { withStyles } from '@material-ui/core/styles';
import styles from './style/styles';

// @see https://material-ui-next.com/api/button/
const colors = ['default', 'inherit', 'primary', 'secondary'];
const sizes = ['small', 'medium', 'large'];
const variants = ['fab', 'flat', 'icon', 'raised'];

/**
 * @param {Array} haystack
 * @param {Array} needles
 * @return {string|undefined}
 */
export function oneOf(haystack, needles) {
  const [result, overflow] = needles
    .filter(name => haystack.includes(name));

  if (result && !overflow) {
    return result;
  }
}

/**
 * @param {Array} presets
 * @return {Object}
 */
export const parsePresets = presets => ({
  color: oneOf(colors, presets),
  fullWidth: presets.includes('fullWidth'),
  mini: presets.includes('mini'),
  size: oneOf(sizes, presets),
  variant: oneOf(variants, presets),
});

function getByType(value, type) {
  if (typeof value == type) {
    return value;
  }
}

/**
 * @param action
 * @return {Object}
 */
export const parseAction = action => ({
  href: getByType(action, 'string'),
  onClick: getByType(action, 'function'),
});

/**
 * @todo Add better validation of icon props
 *
 * @param {Object} props
 * @param {Function|string} props.action
 * @param {*} props.children
 * @param {Object} props.classes
 * @param {boolean} [props.disabled=false]
 *   [Disabled State](https://html.spec.whatwg.org/multipage/interactive-elements.html#command-facet-disabledstate)
 * @param {Array} [props.presets=[]]
 *   color, fullWidth, mini, size and variant
 * @return {ReactElement}
 */
function Button({
  action,
  children,
  classes,
  disabled = false,
  presets = [],
  overrides = {},
}) {
  const props = parsePresets(presets);

  if (props.variant === 'fab') {
    return (
      <MuiButton
        color={props.color}
        onClick={action}
        variant="fab"
      >
        <Icon>{children}</Icon>
      </MuiButton>
    );
  }

  if (props.variant === 'icon') {
    return (
      <IconButton
        classes={overrides}
        color={props.color}
        disabled={disabled}
        onClick={action}
      >
        <Icon>{children}</Icon>
      </IconButton>
    );
  }

  return (
    <MuiButton
      {...parseAction(action)}
      {...props}
      classes={classes}
      disabled={disabled}
    >
      {children}
    </MuiButton>
  );
}

export default withStyles(styles)(Button);
