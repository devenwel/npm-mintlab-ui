import {
  React,
  stories,
  action,
  array,
  boolean,
  event,
  text,
} from '../../story';
import Button from '.';

stories(module, __dirname, {
  Button() {
    return (
      <Button
        action={action('Click')}
        disabled={boolean('Disabled', false)}
        presets={array('Presets', [])}
      >
        {text('Greeting', 'Hello, world!')}
      </Button>
    );
  },
  Link() {
    return (
      <Button
        action={text('URL', 'https://mintlab.nl')}
        disabled={boolean('Disabled', false)}
        presets={array('Presets', ['primary', 'large', 'raised'])}
      >
        {text('Greeting', 'Hello, world!')}
      </Button>
    );
  },
  ['Floating Action Button']: function FloatingActionButton() {
    return (
      <Button
        action={event(Button)}
        disabled={boolean('Disabled', false)}
        presets={['secondary', 'fab']}
      >add</Button>
    );
  },
  Icon() {
    return (
      <Button
        action={event(Button)}
        disabled={boolean('Disabled', false)}
        presets={['primary', 'icon']}
      >alarm</Button>
    );
  },
});
