import React from 'react';
import Button from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `Button` component', () => {

  test('needs tests', () => {
    const actual = (
      <Button/>
    );
    const expected = renderToStaticMarkup('');

    expect(actual).toBe(expected);
  });

});
