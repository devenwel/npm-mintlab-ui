/**
* Button styles. Covers 'raised' variant
*
* @param {Object} theme
* @return {Object}
*/
const styles = ({
  palette: {
    primary,
    secondary,
    common,
  },
  shape,
  typography,
}) => ({
  raised: {
    borderRadius: shape.buttonBorderRadius,
    color: common.white,
    textTransform: 'none',
    boxShadow: 'none',
    fontWeight: typography.fontWeightRegular,
  },
  raisedPrimary: {
    backgroundColor: primary.main,
  },
  raisedSecondary: {
    backgroundColor: secondary.main,
  },
});

export default styles;
