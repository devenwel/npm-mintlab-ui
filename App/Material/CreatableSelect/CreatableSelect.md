# `CreatableSelect` component

>Wrapper component for `React Select v2` - [documentation](https://deploy-preview-2289--react-select.netlify.com/home)

Takes a Form Field definition as props and renders Creatable React Select. This behaves as a textfield where values can be added and removed.

If provided, the `values` prop may be an array 
of objects:

    [{
      value: 1,
      label: 'Test item label 1',
    },
    {
      value: 2,
      label: 'Test item label 2',
    }]

or a single object.