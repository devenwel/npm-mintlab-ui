import React, { Component } from 'react';
import ReactSelectCreatable from 'react-select/lib/Creatable';
import { components } from 'react-select';
import Render from '../../Abstract/Render';
import { bind } from '../../library';
import { withStyles } from '@material-ui/core/styles';
import ErrorLabel from '../../Zaaksysteem/Form/ErrorLabel/ErrorLabel';
import Icon from '../Icon/Icon';
import getStyles from '../Select/style/reactSelect';
import materialUI from '../Select/style/materialUI';

const MSG_CREATABLE = 'Typ, en <ENTER> om te bevestigen.';
const MSG_CREATE = 'Aanmaken:';

/**
 * Wrapper for Creatable React Select v2.
 * Additional props will be passed to that component.
 *
 * @see https://deploy-preview-2289--react-select.netlify.com/home
 * @see https://zaaksysteem.gitlab.io/npm-mintlab-ui/storybook/?selectedKind=Material/CreatableSelect
 *
 * @reactProps {string} error
 * @reactProps {string} name
 * @reactProps {Function} onBlur
 * @reactProps {Function} onChange
 * @reactProps {Object} translations
 * @reactProps {Object|Array} value
 * @reactProps {Object} styles
 * @reactProps {Object} components
 */

class CreatableSelect extends Component {
  /**
   * @ignore
   */

  static get defaultProps() {
    return {
      translations: {
        'form:creatable': MSG_CREATABLE,
        'form:create': MSG_CREATE,
      },
      disabled: false,
    };
  }

  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);

    const { value } = props;

    this.state = {
      value,
      hasFocus: false,
    };

    bind(this, 'handleBlur', 'handleChange', 'handleFocus', 'ClearIndicator');
  }

  /**
   * @return {ReactElement}
   */
  /* eslint-disable no-unused-vars */
  render() {
    const {
      props: {
        error,
        translations,
        config,
        name,
        onBlur,
        onChange,
        onFocus,
        theme,
        styles,
        value,
        components,
        disabled,
        ...rest
      },
      state: {
        value:currentValue,
        hasFocus,
      },
      handleChange,
      handleBlur,
      handleFocus,
      ClearIndicator,
    } = this;

    const createLabel = input => 
      [translations['form:create'], ' ', input].join('');

    return (
      <div>
        <ReactSelectCreatable
          isDisabled={disabled}
          isMulti={true}
          components={{
            DropdownIndicator: null,
            ClearIndicator,
          }}
          defaultValue={currentValue}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          placeholder={translations['form:creatable']}
          formatCreateLabel={createLabel}
          noOptionsMessage={() => translations['form:creatable']}
          styles={styles || getStyles({ theme, error, hasFocus })}
          {...rest}
        />
        <Render
          condition={ Boolean(error && hasFocus) }
        >
          <ErrorLabel label={error} />
        </Render>
      </div>
    );
  }

  /**
   * Call form change function, if provided
   * @param {*} value
   */
  handleChange(value) {
    const { onChange, name } = this.props;

    this.setState({
      value,
    });

    if (!onChange) return;

    onChange({
      target: {
        name,
        value,
        type: 'creatable',
      },
    });
  }

  /**
   * Call form blur function, if provided
   * @param {Event} event
   */
  handleBlur(event) {
    const {
      props: { onBlur, name },
      state: { value },
    } = this;

    event.stopPropagation();

    this.setState({
      hasFocus: false,
    });

    if (!onBlur) return;

    onBlur({
      target: {
        name,
        value,
        type: 'creatable',
      },
    });
  }

  /**
   * Call form focus function, if provided
   * @param {Event} event
   */
  handleFocus(event) {
    const { props: { onFocus, name }} = this;
    event.stopPropagation();

    this.setState({
      hasFocus: true,
    });

    if (!onFocus) return;
    
    onFocus({
      target: {
        name,
      },
    });
  }

  /**
   * @param {*} parentProps
   * @return {ReactElement}
   */
  ClearIndicator(parentProps) {
    const { clearIndicator } = this.props.classes;

    return (
      <components.ClearIndicator {...parentProps}>
        <Icon classes={{ root: clearIndicator}}>close</Icon>
      </components.ClearIndicator>
    );
  }
}

export default withStyles(materialUI, {
  withTheme: true,
})(CreatableSelect);
