import {
  React,
  stories,
  text,
} from '../../story';
import CreatableSelect from '.';

const value = [
  {
    value: 'chocolate',
    label: 'Chocolate',
  },
  {
    value: 'strawberry',
    label: 'Strawberry',
  },
  {
    value: 'vanilla',
    label: 'Vanilla',
  },
];

stories(module, __dirname, {
  Default() {
    return (
      <CreatableSelect
        value={value}
        error={text('Error', '')}
      />
    );
  },
});
