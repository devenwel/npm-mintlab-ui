import { traverseMap } from '../../../library';

/**
   * @param {Object} parameters
   * @param {Object} parameters.theme
   * @param {string} parameters.error
   * @param {boolean} parameters.hasFocus
   */
const getStyles = ({
  theme: {
    palette: {
      common,
      primary,
      grey,
      error:errorPalette,
      text,
    },
    typography: {
      fontFamily,
    },
    zIndex,
  },
  error,
  hasFocus,
}) => {
  function backgroundColorGeneral() {
    return (error && !hasFocus) ? errorPalette.light : grey[grey.defaultWeight];
  }
  function backgroundColorOption(state) {
    const valueMap = new Map([
      [() => state.isSelected, () => grey[grey.defaultWeight]],
      [() => state.isFocused, () => primary.light],
      [() => true, () => common.white],
    ]);
    return traverseMap(valueMap);
  }
  function borderBottom() {
    if (!error && hasFocus) {
      return `2px solid ${primary.main}`;
    }
  }

  return {
    container(base) {
      return {
        ...base,
        width: '100%',
        fontFamily,
      };
    },
    indicatorSeparator(base) {
      return {
        ...base,
        display: 'none',
      };
    },
    control(base) {
      return {
        ...base,
        boxShadow: 'none',
        border: 'none',
        borderBottom: borderBottom(),
        backgroundColor: backgroundColorGeneral(),
      };
    },
    valueContainer(base) {
      return {
        ...base,
        backgroundColor: backgroundColorGeneral(),
      };
    },
    option(base, state) {
      return {
        ...base,
        backgroundColor: backgroundColorOption(state),
        color: text.primary,
      };
    },
    multiValue(base) {
      return {
        ...base,
        borderRadius: '12px',
        padding: '2px',
      };
    },
    input(base) {
      return {
        ...base,
        input: {
          fontFamily,
        },
      };
    },
    multiValueRemove(base) {
      const GREY_COLOR = 300;
      const GREY_BGCOLOR = 500;

      return {
        ...base,
        color: grey[GREY_COLOR],
        backgroundColor: grey[GREY_BGCOLOR],
        borderRadius: '50%',
        transform: 'scale(0.85)',
      };
    },
    menu(base) {
      return {
        ...base,
        zIndex: zIndex.options,
      };
    },
  };
};

export default getStyles;
