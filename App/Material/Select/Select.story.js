import {
  React,
  stories,
  text,
  boolean,
} from '../../story';
import Select from '.';

const choices = [
  {
    value: 'chocolate',
    label: 'Chocolate',
    alternativeLabels: ['Yummy', 'I love this flavor'],
  },
  {
    value: 'strawberry',
    label: 'Strawberry',
    alternativeLabels: ['Fruit', 'Red'],
  },
  {
    value: 'vanilla',
    label: 'Vanilla',
  },
];

const [, defaultValue] = choices;

stories(module, __dirname, {
  Default() {
    return (
      <Select
        choices={choices}
        value={defaultValue}
        error={text('Error', '')}
        isMulti={boolean('Multiple choices', false)}
      />
    );
  },
});
