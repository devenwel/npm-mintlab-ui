# `Select` component

>Wrapper component for `React Select v2` - [documentation](https://deploy-preview-2289--react-select.netlify.com/home)

Takes a Form Field definition as props and renders React Select. If provided, the `choices` prop must be an array 
of objects:

    [{
      value: 1,
      label: 'Test item label 1',
    },
    {
      value: 2,
      label: 'Test item label 2',
    }]

If `config.getChoices` is provided, it will be called with the input box's value after the user has typed something
in the input field, to facilitate autocomplete behaviour. 