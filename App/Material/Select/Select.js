import React, { Component } from 'react';
import ReactSelect, { components } from 'react-select';
import Render from '../../Abstract/Render';
import { bind } from '../../library';
import { withStyles } from '@material-ui/core/styles';
import ErrorLabel from '../../Zaaksysteem/Form/ErrorLabel/ErrorLabel';
import getStyles from './style/reactSelect';
import Icon from '../Icon/Icon';
import materialUI from './style/materialUI';

const DELAY = 400;
const MIN_CHARACTERS = 3;
const MSG_CHOOSE = 'Select…';
const MSG_LOADING = 'Loading…';
const MSG_TYPE = 'Begin typing to search…';

/**
 * Wrapper for React Select v2.
 * Additional props will be passed through to that component.
 *
 * @see https://deploy-preview-2289--react-select.netlify.com/home
 * @see https://zaaksysteem.gitlab.io/npm-mintlab-ui/storybook/?selectedKind=Material/Select
 *
 * @reactProps {boolean} autoLoad=false
 * @reactProps {Array} choices
 * @reactProps {Object} config
 * @reactProps {boolean} disabled=false
 * @reactProps {Function} getChoices
 * @reactProps {string} error
 * @reactProps {string} name
 * @reactProps {Function} onBlur
 * @reactProps {Function} onChange
 * @reactProps {Function} onFocus
 * @reactProps {Object} styles
 * @reactProps {Object} translations
 * @reactProps {*} value
 *
 * Choices and value must be provided in the form of a single, or array of objects:
 * @example
 * {
    value: 'strawberry',
    label: 'Strawberry',
    alternativeLabels: ['Fruit', 'Red'],
  }
 *
 */
class Select extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      translations: {
        'form:choose': MSG_CHOOSE,
        'form:loading': MSG_LOADING,
        'form:beginTyping': MSG_TYPE,
      },
      disabled: false,
      autoLoad: false,
    };
  }

  /**
   * @param {Object} props
   */
  constructor(props) {
    const { value, choices } = props;

    super(props);
    this.state = {
      selected: value,
      isLoading: false,
      hasInitialChoices: (choices && choices.length),
      hasFocus: false,
    };
    this.timeoutId = null;
    bind(this, 'handleBlur', 'handleChange', 'handleFocus', 'handleInputChange', 'DropdownIndicator', 'ClearIndicator');
  }

  /**
   * Lifecycle methods
   */
  componentDidMount() {
    if (this.props.autoLoad) {
      this.getChoices('');
    }
  }

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps.choices) !== JSON.stringify(this.props.choices)) {
      this.setState({
        isLoading: false,
      });
    }
  }

  /**
   * @return {ReactElement}
   */
  /* eslint-disable no-unused-vars */
  render() {
    const {
      props: {
        choices:options,
        error,
        translations,
        theme,
        config,
        value,
        onChange,
        onBlur,
        onFocus,
        disabled,
        autoLoad,
        classes,
        name,
        styles,
        ...rest
      },
      state: {
        selected,
        isLoading,
        hasInitialChoices,
        hasFocus,
      },
      handleInputChange,
      handleFocus,
      handleChange,
      handleBlur,
      DropdownIndicator,
      ClearIndicator,
    } = this;

    return (
      <div>
        <ReactSelect
          isDisabled={disabled}
          cacheOptions={true}
          isLoading={ (options && !options.length) ? false : isLoading }
          defaultValue={selected}
          options={options}
          loadingMessage={() => translations['form:loading']}
          noOptionsMessage={() => translations['form:beginTyping']}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          onInputChange={!autoLoad && !hasInitialChoices && handleInputChange}
          placeholder={translations['form:choose']}
          styles={styles || getStyles({ theme, error, hasFocus })}
          classNamePrefix='react-select'
          name={name}
          filterOption={this.filterOption}
          components={{
            DropdownIndicator,
            ClearIndicator,
          }}
          {...rest}
        />
        <Render
          condition={ Boolean(error && hasFocus) }
        >
          <ErrorLabel label={error} />
        </Render>
      </div>
    );
  }

  /**
   * Call form change function, if provided.
   *
   * @param {Object} selected
   */
  handleChange(selected) {
    this.setState({
      selected,
    });

    const { onChange, name } = this.props;

    if (!onChange) return;

    onChange({
      target: {
        name,
        value: selected,
        type: 'select',
      },
    });
  }

  /**
   * Call form blur function, if provided.
   *
   * @param {Event} event
   */
  handleBlur(event) {
    const {
      props: {
        onBlur,
        name,
      },
      state: { selected },
    } = this;
    event.stopPropagation();

    this.setState({
      hasFocus: false,
    });

    if (!onBlur) {
      return;
    }

    onBlur({
      target: {
        name,
        value: selected,
        type: 'select',
      },
    });
  }

  /**
   * Call form focus function, if provided
   *
   * @param {Event} event
   */
  handleFocus(event) {
    const { props: { onFocus, name }} = this;
    event.stopPropagation();

    this.setState({
      hasFocus: true,
    });

    if (!onFocus) return;

    onFocus({
      target: {
        name,
      },
    });
  }

  /**
   * @param {*} input
   */
  isValidInput(input) {
    return (input && (input.length >= MIN_CHARACTERS));
  }

  /**
   * @param {*} input
   */
  handleInputChange(input) {
    if (!this.isValidInput(input)) {
      return;
    }

    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }

    this.timeoutId = setTimeout(() => {
      this.getChoices(input);
    }, DELAY);
  }

  /**
   * @param {*} input
   */
  getChoices(input) {
    const { getChoices } = this.props;

    if (!getChoices) {
      return;
    }

    this.setState({
      isLoading: true,
    });

    getChoices(input);
  }

  /**
   * @param {*} parentProps
   * @return {ReactElement}
   */
  DropdownIndicator(parentProps) {
    const { choices } = this.props;
    const { selectProps: { menuIsOpen }} = parentProps;

    const onClick = () => {
      if ((!choices && (Array.isArray(choices) && !choices.length)) ||
        !menuIsOpen
      ) {
        return;
      }

      this.getChoices('');
    };

    return (
      <div onClick={onClick}>
        <components.DropdownIndicator {...parentProps} >
          <Icon
            color='inherit'
          >arrow_drop_down</Icon>
        </components.DropdownIndicator>
      </div>
    );
  }

  ClearIndicator(parentProps) {
    const { clearIndicator } = this.props.classes;

    return (
      <components.ClearIndicator {...parentProps}>
        <Icon classes={{ root: clearIndicator}}>close</Icon>
      </components.ClearIndicator>
    );
  }

  /**
   * Custom filter function that also filters on alternative labels
   * @param {string} option
   * @param {string} input
   * @return {boolean}
   */
  filterOption(option, input) {
    if (!input) {
      return true;
    }

    const { label, alternativeLabels } = option.data;
    const labels = [label]
      .concat(alternativeLabels)
      .filter(l => l);
    const clean = value => value.trim().toLowerCase();

    return labels.some(thisLabel =>
      clean(thisLabel).includes(clean(input))
    );

  }
}

export default withStyles(materialUI, {
  withTheme: true,
})(Select);
