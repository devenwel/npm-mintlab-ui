export const styles = () => ({
  flex: {
    flexGrow: 1,
    padding: '0 1rem',
  },
});
