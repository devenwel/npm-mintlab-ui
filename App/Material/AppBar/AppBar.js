import React from 'react';
import MuiAppBar from '@material-ui/core/AppBar';
import MuiToolbar from '@material-ui/core/Toolbar';
import Button from '../Button';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './style/styles';

const AppBar = ({
  children,
  classes,
  className,
  gutter,
  onMenuClick,
  onUserClick,
}) => (
  <MuiAppBar
    className={className}
    color="inherit"
    position="absolute"
  >
    <MuiToolbar
      disableGutters={true}
    >
      <div
        style={{
          padding: `0 ${gutter}px`,
        }}
      >
        <Button
          action={onMenuClick}
          presets={['icon']}
        >menu</Button>
      </div>
      <div
        className={classes.flex}
      >
        {children}
      </div>
      <div
        style={{
          padding: `0 ${gutter}px`,
        }}
      >
        <Button
          action={onUserClick}
          presets={['icon']}
        >account_circle</Button>
      </div>
    </MuiToolbar>
  </MuiAppBar>
);

export default withStyles(styles)(AppBar);
