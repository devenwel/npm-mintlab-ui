import React from 'react';
import AppBar from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `AppBar` component', () => {

  test('creates the expected HTML', () => {
    const actual = renderToStaticMarkup(<AppBar>TEST</AppBar>);
    const expected = '<div>TEST</div>';

    expect(actual).toBe(expected);
  });

});
