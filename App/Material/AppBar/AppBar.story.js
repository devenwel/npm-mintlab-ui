import {
  React,
  stories,
  text,
  action,
} from '../../story';
import AppBar from '.';

stories(module, __dirname, {
  Default() {
    return (
      <AppBar
        gutter={10}
        onMenuClick={action('Menu')}
        onUserClick={action('User')}
      >
        {text('Greeting', 'Hello, world!')}
      </AppBar>
    );
  },
});
