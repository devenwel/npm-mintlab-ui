import {
  React,
  stories,
  text,
} from '../../story';
import Card from '.';

stories(module, __dirname, {
  Default() {
    return (
      <Card title={text('Title', 'Card title')} description={text('Description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.')}>
        {text('Content', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam et tortor felis. Nunc vel gravida turpis, mattis venenatis nibh. Praesent sapien arcu, laoreet in varius at, consequat eu elit. Morbi magna nunc, luctus eu tortor sed, mollis cursus urna. Vivamus placerat odio non bibendum molestie. Fusce molestie rutrum felis at sodales. Curabitur sollicitudin, nibh eu fermentum tempus, libero turpis condimentum sem, quis efficitur urna est ullamcorper mauris. Pellentesque a libero nec tortor rhoncus tristique. Fusce pellentesque venenatis erat. Quisque in rutrum nunc. Nunc bibendum est a viverra iaculis.')}
      </Card>
    );
  },
});
