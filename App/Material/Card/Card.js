import React from 'react';
import MuiCard from '@material-ui/core/Card';
import MuiCardContent from '@material-ui/core/CardContent';
import MuiCardHeader from '@material-ui/core/CardHeader';
import { withStyles } from '@material-ui/core/styles';
import Render from '../../Abstract/Render';
import styles from './style/styles';

/**
 * Wrapper for Material UI Card
 * Children will be rendered in the Card Content, additional props will be passed through.
 */
const Card = props => {
  const {
    classes,
    contentClasses,
    children,
    title,
    description,
    ...rest
  } = props;
  
  return (
    <MuiCard
      classes={classes}
      {...rest}
    >
      <Render condition={title}>
        <MuiCardHeader title={title} subheader={description} />
      </Render>
    
      <MuiCardContent
        classes={contentClasses}
      >
        { children }
      </MuiCardContent>
    </MuiCard>
  );
};

export default withStyles(styles)(Card);
