/** 
* @param {Object} theme
*/
const styles = () => ({
  root: {
    marginBottom: '25px',
    overflow: 'visible',
  },
});

export default styles;
