import React from 'react';
import Icon from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `Icon` component', () => {

  test('needs tests', () => {
    const actual = (
      <Icon/>
    );
    const expected = renderToStaticMarkup('');

    expect(actual).toBe(expected);
  });

});
