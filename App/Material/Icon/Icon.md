# `Icon` component

> *Material Design* `Icon` component.

- [Official Icons](https://material.io/tools/icons/)
- [Community Icons](https://materialdesignicons.com/)
