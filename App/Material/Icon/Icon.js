import { createElement } from 'react';
import * as iconMap from './library';

/**
 * Icon component.
 *
 * @example
 * <Icon color="red" name="menu"/>
 *
 * @param {Object} props
 * @param {string} props.children
 * @param {Object} [props.classes]
 * @param {string} [props.color]
 * @return {ReactElement}
 */
const Icon = ({ children, classes, color }) =>
  createElement(iconMap[children], {
    classes,
    color,
  });

export default Icon;
