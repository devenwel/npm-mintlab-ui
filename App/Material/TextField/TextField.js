import React, { Component } from 'react';
import MuiTextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import { bind } from '../../library';
import classNames from 'classnames';
import styles from './style/styles';

const DEFAULT_ROWS = 3;

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Object} props.config
 * @param {boolean} [props.disabled=false]
 * @param {string} [props.error]
 * @param {string} [props.info]
 * @param {boolean} [props.required=false]
 * @param {string} props.label
 * @param {string} props.name
 * @param {number} props.rows
 * @param {string} props.value
 * @return {ReactElement}
 */

class TextField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasFocus: false,
    };

    bind(this, 'handleBlur', 'handleFocus');
  }

  render() {
    const {
      props: {
        classes,
        disabled = false,
        error,
        info,
        label,
        name,
        required = false,
        config,
        rows = DEFAULT_ROWS,
        value,
      },
      state: {
        hasFocus,
      },
      handleBlur,
      handleFocus,
      stylingProps,
    } = this;

    return (
      <MuiTextField
        defaultValue={value}
        disabled={disabled}
        error={Boolean(error)}
        helperText={error || info}
        label={label}
        name={name}
        required={required}
        multiline={config && config.style === 'paragraph'}
        rows = {rows}
        onBlur={handleBlur}
        onFocus={handleFocus}
        {...stylingProps({ classes, error, hasFocus })}
      />
    );
  }

  /**
   * @param {Event} event
   */
  handleBlur() {
    this.setState({
      hasFocus: false,
    });
  }

  /**
   * @param {Event} event
   */
  handleFocus() {
    this.setState({
      hasFocus: true,
    });
  }

  /**
   * @param {*} parameters
   * @param {Object} parameters.classes
   * @param {string} parameters.error
   * @param {boolean} hasFocus
   * @return {Object}
   */
  stylingProps({ classes, error, hasFocus }) {
    const {
      inputClass,
      inputErrorClass,
      formHelperErrorClass,
      inputLabelClass,
      inputLabelShrinkClass,
      inputLabelInactiveClass,
      inputLabelErrorClass,
      formControlClass,
    } = classes;

    const errorState = Boolean(error && !hasFocus);

    return {
      classes: {
        root: formControlClass,
      },
      InputProps: {
        classes: {
          input: classNames(inputClass, { [inputErrorClass]: errorState }),
        },
        disableUnderline: errorState,
      },
      FormHelperTextProps: {
        classes: {
          root: classNames({ [formHelperErrorClass]: errorState }),
        },
      },
      InputLabelProps: {
        classes: {
          root: classNames(inputLabelClass, inputLabelInactiveClass, { [inputLabelErrorClass]: errorState }),
          shrink: classNames(inputLabelClass, inputLabelShrinkClass, { [inputLabelErrorClass]: errorState }),
        },
      },
    };
  }
}

export default withStyles(styles)(TextField);
