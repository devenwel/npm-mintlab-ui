
/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the TextField component
 * @param {Object} theme
 * @return {Object}
 */
const styles = ({
  palette: {
    grey,
    error,
    text,
  },
  shape: {
    borderRadius,
  },
  zIndex: {
    inputLabel,
  },
}) => ({
  // Form Control
  formControlClass: {
    width: '100%',
  },
  // Form Helper
  formHelperErrorClass: {
    display: 'none',
  },
  // Input
  inputClass: {
    padding: '20px 14px 10px 14px',
    borderRadius,
    backgroundColor: grey[grey.defaultWeight],
  },
  inputErrorClass: {
    backgroundColor: error.light,
  },
  // Label
  inputLabelClass: {
    zIndex: inputLabel,
    pointerEvents: 'none',
    left: '12px',
  },
  inputLabelShrinkClass: {
    transform: 'translate(0, 16px)',
    fontSize: '9px',
  },
  inputLabelInactiveClass: {
    top: '9px',
  },
  inputLabelErrorClass: {
    color: `${text.secondary} !important`,
  },
});

export default styles;
