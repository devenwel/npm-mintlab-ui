import React from 'react';
import TextField from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `TextField` component', () => {

  test('needs tests', () => {
    const actual = (
      <TextField/>
    );
    const expected = renderToStaticMarkup('');

    expect(actual).toBe(expected);
  });

});
