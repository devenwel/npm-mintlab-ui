import React from 'react';
import Checkbox from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `Checkbox` component', () => {

  test('needs tests', () => {
    const actual = (
      <Checkbox/>
    );
    const expected = renderToStaticMarkup('');

    expect(actual).toBe(expected);
  });

});
