import {
  React,
  stories,
  select,
} from '../../story';
import Checkbox from '.';

stories(module, __dirname, {
  Default() {
    return (
      <Checkbox
        checked={false}
        label="test"
        name="foobar"
        color={select('Color', ['primary', 'secondary', 'default'], 'primary')}
      />
    );
  },
});
