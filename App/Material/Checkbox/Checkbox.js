import React from 'react';
import MuiCheckbox from '@material-ui/core/Checkbox';
import MuiFormControlLabel from '@material-ui/core/FormControlLabel';

/**
 * @param {Object} props
 * @param {boolean} [props.defaultChecked=false]
 * @param {boolean} [props.disabled=false]
 * @param {string} [props.label]
 * @param {string} [props.name]
 * @param {string} [props.color='primary']
 * @return {ReactElement}
 */
const Checkbox = ({
  defaultChecked = false,
  disabled,
  label,
  name,
  color='primary',
}) => (
  <MuiFormControlLabel
    control={(
      <MuiCheckbox
        defaultChecked={defaultChecked}
        disabled={disabled}
        name={name}
        color={color}
      />
    )}
    label={label}
  />
);

export default Checkbox;
