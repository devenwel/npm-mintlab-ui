import React, { Component } from 'react';
import Radio from '@material-ui/core/Radio';
import MuiRadioGroup from '@material-ui/core/RadioGroup';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import { bind } from '../../library';

/**
 * @param {Object} props
 * @param {boolean} [props.disabled=false]
 * @param {string} props.label
 * @param {string} props.value
 * @param {number} index
 * @return {ReactElement}
 */
const Choice = ({
  disabled = false,
  label,
  value,
}, index) => (
  <FormControlLabel
    key={index}
    control={<Radio/>}
    disabled={disabled}
    label={label}
    value={value}
  />
);

/**
 * ZS-FIXME: it's unclear if and how this could be uncontrolled
 *
 * @reactProps {Array} props.choices
 * @reactProps {boolean} [props.disabled=false]
 * @reactProps {string} [props.error='']
 * @reactProps {string} props.label
 * @reactProps {string} props.value
 */
export default class RadioGroup extends Component {
  static get defaultProps() {
    return {
      disabled: false,
      error: '',
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      // ZS-FIXME: prefix with 'default' in all components
      value: props.value,
    };
    bind(this, 'handleChange');
  }

  /**
   * @return {ReactElement}
   */
  render() {
    const {
      props: {
        choices,
        error,
        label,
        name,
        required,
      },
      state: {
        value,
      },
    } = this;

    return (
      <FormControl
        component="fieldset"
        error={Boolean(error)}
        required={required}
      >
        <FormLabel
          component="legend"
        >{label}</FormLabel>
        <MuiRadioGroup
          name={name}
          value={value}
          onChange={this.handleChange}
        >
          {choices.map(Choice)}
        </MuiRadioGroup>
        <FormHelperText>{error}</FormHelperText>
      </FormControl>
    );
  }

  handleChange({
    target: {
      value,
    },
  }) {
    this.setState({ value });
  }

}
