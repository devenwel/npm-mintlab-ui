import React from 'react';
import RadioGroup from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `RadioGroup` component', () => {

  test('needs tests', () => {
    const actual = (
      <RadioGroup/>
    );
    const expected = renderToStaticMarkup('');

    expect(actual).toBe(expected);
  });

});
