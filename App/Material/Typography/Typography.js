export { Display4 } from './style/Display4';
export { Display3 } from './style/Display3';
export { Display2 } from './style/Display2';
export { Display1 } from './style/Display1';
export { Headline } from './style/Headline';
export { Title } from './style/Title';
export { Subheading } from './style/Subheading';
export { Body2 } from './style/Body2';
export { Body1 } from './style/Body1';
export { Caption } from './style/Caption';
// This would conflict with the Button component.
// The convenience of just importing the other styles by
// their Material Design style name is related to the
// use case probability.
export { Button as ButtonText } from './style/Button';

export { withTypography } from './library/withTypography';
