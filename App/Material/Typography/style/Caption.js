import React from 'react';
import Typography from '@material-ui/core/Typography';

/**
 * @see https://material-ui-next.com/api/typography/
 *
 * @param {Object} props
 * @param {Array} props.children
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const Caption = ({ 
  children,
  classes,
}) => (
  <Typography
    variant="caption"
    classes={classes}
  >
    {children}
  </Typography>
);
