import React from 'react';
import Typography from '@material-ui/core/Typography';

/**
 * @see https://material-ui-next.com/api/typography/
 *
 * @param {Object} props
 * @param {Array} props.children
 * @return {ReactElement}
 */
export const Display2 = ({ children }) => (
  <Typography
    variant="display2"
  >
    {children}
  </Typography>
);
