import React from 'react';
import Typography from '@material-ui/core/Typography';

/**
 * @see https://material-ui-next.com/api/typography/
 *
 * @param {Object} props
 * @param {Array} props.children
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const Body2 = ({
  children,
  classes,
}) => (
  <Typography
    variant="body2"
    classes={classes}
  >
    {children}
  </Typography>
);
