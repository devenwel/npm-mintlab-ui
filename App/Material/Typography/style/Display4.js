import React from 'react';
import Typography from '@material-ui/core/Typography';

/**
 * @see https://material-ui-next.com/api/typography/
 *
 * @param {Object} props
 * @param {Array} props.children
 * @return {ReactElement}
 */
export const Display4 = ({ children }) => (
  <Typography
    variant="display4"
  >
    {children}
  </Typography>
);
