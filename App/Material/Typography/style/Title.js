import React from 'react';
import Typography from '@material-ui/core/Typography';

/**
 * @see https://material-ui-next.com/api/typography/
 *
 * @param {Object} props
 * @param {Array} props.children
 * @return {ReactElement}
 */
export const Title = ({ children }) => (
  <Typography
    variant="title"
  >
    {children}
  </Typography>
);
