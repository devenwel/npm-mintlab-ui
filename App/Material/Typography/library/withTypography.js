import { withStyles } from '@material-ui/core/styles';

const getTypograpy = theme => theme.typography;

const enhance = withStyles(getTypograpy);

export const withTypography = Component =>
  enhance(Component);
