# `Typography` component

>

## Export

This module exposes named exports that correspond with
[Material Design Typography Styles](https://material.io/guidelines/style/typography.html#typography-styles):

- Display4
- Display3
- Display2
- Display1
- Headline
- Title
- Subheading
- Body2
- Body1
- Caption
- ButtonStyle
