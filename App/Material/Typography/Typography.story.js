import {
  React,
  stories,
  text,
} from '../../story';
import {
  Display4,
  Display3,
  Display2,
  Display1,
  Headline,
  Title,
  Subheading,
  Body2,
  Body1,
  Caption,
  ButtonText,
} from '.';
import { withTypography } from './library/withTypography';

const Base = ({
  classes: {
    title,
    button,
  },
}) => (
  <div>
    <span
      className={title}
    >The quick brown fox</span>
    {' '}
    <span
      className={button}
    >jumps over the lazy dog.</span>
  </div>
);

stories(module, __dirname, {
  Display4() {
    return (
      <Display4>
        {text('Greeting', 'Hello, world!')}
      </Display4>
    );
  },
  Display3() {
    return (
      <Display3>
        {text('Greeting', 'Hello, world!')}
      </Display3>
    );
  },
  Display2() {
    return (
      <Display2>
        {text('Greeting', 'Hello, world!')}
      </Display2>
    );
  },
  Display1() {
    return (
      <Display1>
        {text('Greeting', 'Hello, world!')}
      </Display1>
    );
  },
  Headline() {
    return (
      <Headline>
        {text('Greeting', 'Hello, world!')}
      </Headline>
    );
  },
  Title() {
    return (
      <Title>
        {text('Greeting', 'Hello, world!')}
      </Title>
    );
  },
  Subheading() {
    return (
      <Subheading>
        {text('Greeting', 'Hello, world!')}
      </Subheading>
    );
  },
  Body2() {
    return (
      <Body2>
        {text('Greeting', 'Hello, world!')}
      </Body2>
    );
  },
  Body1() {
    return (
      <Body1>
        {text('Greeting', 'Hello, world!')}
      </Body1>
    );
  },
  Caption() {
    return (
      <Caption>
        {text('Greeting', 'Hello, world!')}
      </Caption>
    );
  },
  Button() {
    return (
      <ButtonText>
        {text('Greeting', 'Hello, world!')}
      </ButtonText>
    );
  },
  'With Typography': function enhanceWithTypography() {
    return React.createElement(withTypography(Base));
  },
});
