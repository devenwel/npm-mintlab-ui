const { assign, keys } = Object;
const { stringify } = JSON;

/**
 * Generate a unique identifier.
 *
 * @private
 * @param {string} prefix
 * @return {string}
 */
export const unique = prefix => `${prefix}-${window.performance.now()}`;

/**
 * Get a shallow clone of an object without the specified properties.
 *
 * @private
 * @param {Object} object
 * @param {Array} exclusions
 * @return {Object}
 */
export const cloneWithout = (object, ...exclusions) =>
  keys(object)
    .reduce((accumulator, key) => {
      if (!exclusions.includes(key)) {
        accumulator[key] = object[key];
      }

      return accumulator;
    }, {});

/**
 * Bind an infinite set of methods to a class instance.
 *
 * @private
 * @param {Object} instance
 *   Instance of a class
 * @param {...string} methods
 *   Method names of a class
 */
export function bind(instance, ...methods) {
  const reduceBoundMethods = (accumulator, name) =>
    assign(accumulator, {
      [name]: instance[name].bind(instance),
    });
  const boundMethods = methods.reduce(reduceBoundMethods, {});

  assign(instance, boundMethods);
}

/**
 * Deeply compare values that can be serialized as JSON.
 * Note that the order of object properties is NOT normalized.
 *
 * @private
 * @param {*} leftHandSide
 * @param {*} rightHandSide
 * @return {boolean}
 */
export const equals = (leftHandSide, rightHandSide) =>
  stringify(leftHandSide) === stringify(rightHandSide);

/**
 * Traverse the map, and return the output of the 
 * first value function where the key function is truthy
 * 
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map
 * 
 * @param {Map} map
 * @param {Array} [keyArgs=[]]
 * @param {Array} [functionArgs=[]]
 */
export const traverseMap = (map, keyArgs=[], functionArgs=[]) => {
  for (let [mapKey, mapFunction] of map) {
    if(mapKey(...keyArgs)) {
      return mapFunction(...functionArgs);
    }
  }
};
