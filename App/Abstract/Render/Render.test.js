import React from 'react';
import Render from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `Render` component', () => {

  test('needs tests', () => {
    const actual = (
      <Render/>
    );
    const expected = renderToStaticMarkup('');

    expect(actual).toBe(expected);
  });

});
