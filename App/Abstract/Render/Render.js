/**
 * @see https://zaaksysteem.gitlab.io/npm-mintlab-ui/storybook/?selectedKind=Abstract/Render
 *
 * @param {Object} props
 * @param {ReactElement} props.children
 * @param {boolean} props.condition
 * @return {ReactElement|null}
 * @constructor
 */
const Render = ({ children, condition }) => {
  if (condition) {
    return children;
  }

  return null;
};

export default Render;
