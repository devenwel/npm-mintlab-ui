# 🔌 `Render` component

> Use a *JSX* component instead of (yet) an(other) expression 
  to render a component if `condition` is `true`.

- [API documentation](https://zaaksysteem.gitlab.io/npm-mintlab-ui/documentation/identifiers.html#abstract-render)

Instead of

    <div>
      {condition && <SomeComponent/>}
    </div>

use

    <div>
      <Render condition={true}>
        <SomeComponent/>
      </Render>
    </div>

