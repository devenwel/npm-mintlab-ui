# 🔌 `Clone` component

> Use a *JSX* component instead of (yet) an(other) expression 
  to clone properties to a component’s first child.

- [API documentation](https://zaaksysteem.gitlab.io/npm-mintlab-ui/documentation/identifiers.html#abstract-clone)

### JSX expression 

    <div>
      {children.map(child =>
        cloneElement(child, {
          foo: 'foo',
          bar: 'bar',
        }))}
    </div>

### JSX `Clone` component

    import { Clone } from '@mintlab/ui';

#### Clone first child with `props`

    <div>
      <Clone 
        bar="bar"
        foo="foo"
      >
        <SomeComponent/>
      </Clone>
    </div>

## Shorthand method for all children

A component can only return a single Virtual DOM element.
This is syntactic sugar for the expression above.

    import { clone } from '@mintlab/ui';
    
### JSX
    
    <div>
      {clone(children, {
        foo: 'foo',
        bar: 'bar',
      }))}
    </div>
