import { cloneElement } from 'react';
import { cloneWithout } from '../../library';

/**
 * ZS-FIXME: not exported in the distribution yet
 */
export const clone = (children, props) =>
  children.map(child => cloneElement(child, props));

/**
 * @see https://zaaksysteem.gitlab.io/npm-mintlab-ui/storybook/?selectedKind=Abstract/Clone
 *
 * @example
 * <Clone
 *   foo="bar"
 * >
 *   <h1>Hello, world!</h1>
 * </Clone>
 *
 * @param {Object} props
 * @param {ReactElement} props.children
 *   Component content nodes
 * @return {ReactElement}
 */
export default function Clone(props) {
  const { children } = props;
  const newProps = cloneWithout(props, 'children');

  return cloneElement(children, newProps);
}
