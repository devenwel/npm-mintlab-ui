import React from 'react';
import Clone from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `Clone` component', () => {

  test('needs tests', () => {
    const actual = (
      <Clone/>
    );
    const expected = renderToStaticMarkup('');

    expect(actual).toBe(expected);
  });

});
