import React from 'react';
import Dynamic from '.';
import { renderToStaticMarkup } from 'react-dom/server';

/**
 * @test {Dynamic}
 */
describe('The `Dynamic` component', () => {

  test('creates an HTML element if the `component` property is a string', () => {
    const actual = renderToStaticMarkup(<Dynamic component="h1">TEST</Dynamic>);
    const expected = '<h1>TEST</h1>';

    expect(actual).toBe(expected);
  });

  test('creates a component if the `component` property is a function', () => {
    const Foo = ({ barf, children }) => (
      <code title={barf}>{children}</code>
    );

    const actual = renderToStaticMarkup(<Dynamic component={Foo} barf="Yes">TEST</Dynamic>);
    const expected = '<code title="Yes">TEST</code>';

    expect(actual).toBe(expected);
  });

});
