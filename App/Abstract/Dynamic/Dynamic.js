import { createElement } from 'react';
import { cloneWithout } from '../../library';

/**
 * @see https://zaaksysteem.gitlab.io/npm-mintlab-ui/storybook/?selectedKind=Abstract/Dynamic
 *
 * @example
 * const Bold = ({ children, title }) => (
 *   <b
 *     title={title}
 *   >{children}</b>
 * );
 * // … usage:
 * <Dynamic
 *   component={Bold}
 *   title="Baldly go…"
 * >Make it so!</Dynamic>
 * // … renders:
 * <b title="Baldly go…">Make it so!</b>
 *
 * @param {Object} props
 * @return {ReactElement}
 */
const DynamicComponent = props => {
  const { component } = props;
  const newProps = cloneWithout(props, 'component');

  return createElement(component, newProps);
};

export default DynamicComponent;
