# 🔌 `Dynamic` component

> Pass a component dynamically with a property.

- [API documentation](https://zaaksysteem.gitlab.io/npm-mintlab-ui/documentation/identifiers.html#abstract-dynamic)

## Usage

### String (HTML elements)

    <Dynamic
      component="span"
      title="Foobar"
    >Hello, world!</Dynamic> 

### Function

    <Dynamic
      component={Fubar}
      title="Foobar"
    >Hello, world!</Dynamic> 
