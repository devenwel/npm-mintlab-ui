import React, { Component } from 'react';
import Clone from '../Clone';
import Loader from '../../Zaaksysteem/Loader';

const { isArray } = Array;
const { keys } = Object;

/**
 * Declarative resource component.
 * Depends on {@link Clone} and {@link Loader}.
 *
 * @see https://zaaksysteem.gitlab.io/npm-mintlab-ui/storybook/?selectedKind=Abstract/Resource
 *
 * @example
 * const ResultView = ({
 *   resource: {
 *     foo,
 *   },
 * }) => (
 *   <ul>
 *     {foo.map(item, index) => (
 *       <li key={index}>
 *         {item}
 *       </li>
 *     )}
 *   </ul>
 * );
 *
 * <Resource
 *  id="foo"
 *  resolve={someApi.getData}
 * >
 *   <ResultView/>
 * </Resource>
 *
 * @reactProps {ReactElement} children
 *   A component that renders the resolved resource(s).
 * @reactProps {Array<string>|string} id
 *   Resource ID specification
 * @reactProps {string} [loader]
 *   Loader type
 * @reactProps {Object} [payload={}]
 *   Resource payload
 * @reactProps {Function} resolve
 *   Resource resolver
 */
export default class Resource extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      payload: {},
    };
  }

  /**
   *  @see https://reactjs.org/docs/react-component.html#componentdidmount
   */
  componentDidMount() {
    const { resolve, id } = this.props;

    resolve(id);
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#unsafe_componentwillreceiveprops
   * @todo refactor deprecated lifecycle method
   *
   * @param {Object} nextProps
   */
  UNSAFE_componentWillReceiveProps(nextProps) {
    const { resolve } = this.props;
    const { id } = nextProps;

    if (this.hasResourceIdChanged(id)) {
      resolve(id);
    }
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const { children, loader, payload } = this.props;

    if (this.isCurrentResourceResolved()) {
      return (
        <Clone
          resource={payload}
        >{children}</Clone>
      );
    }

    return (
      <Loader
        type={loader}
      />
    );
  }

  /**
   * Get a normalized version of the resource ID for comparison.
   *
   * @param {string|Array} id
   * @return {string}
   */
  getIdAsString(id) {
    if (isArray(id)) {
      return id.sort().join();
    }

    return id;
  }

  /**
   * @param {string|Array} nextId
   * @return {boolean}
   */
  hasResourceIdChanged(nextId) {
    const { id } = this.props;

    return (this.getIdAsString(id) !== this.getIdAsString(nextId));
  }

  /**
   * @return {boolean}
   */
  isCurrentResourceResolved() {
    const { payload } = this.props;

    return keys(payload).every(key => (payload[key] !== null));
  }
}
