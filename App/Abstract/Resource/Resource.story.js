import {
  React,
  stories,
  object,
} from '../../story';
import reactTriggerChange from 'react-trigger-change';
import Resource from '.';

const KNOB_ID = 'Resource';
const DELAY = 3000;

const getKnob = id => window.parent.document.getElementById(id);

function setResource(id, data) {
  const textArea = getKnob(KNOB_ID);

  if (textArea) {
    if (data) {
      textArea.value = JSON.stringify({
        [id]: {
          data,
        },
      });
      textArea.disabled = true;
    } else {
      textArea.value = JSON.stringify({
        [id]: null,
      });
      textArea.disabled = false;
    }

    reactTriggerChange(textArea);
  }
}

const delay = {
  resourceTypes: {
    foo: ['FOO RESOURCE'],
    bar: ['BAR RESOURCE'],
  },
  get(id) {
    setResource(id);
    setTimeout(function resolveResource() {
      setResource(id, delay.resourceTypes[id]);
    }, DELAY);
  },
};

const Result = ({ resource }) => (
  <ul>
    {resource.foo.data.map((item, index) => (
      <li key={index}>{item}</li>
    ))}
  </ul>
);

stories(module, __dirname, {
  'Circle Loader': function CircleLoader() {
    return (
      <Resource
        id={['foo']}
        payload={object(KNOB_ID, {
          foo: null,
        })}
        resolve={delay.get}
      >
        <Result/>
      </Resource>
    );
  },
});
