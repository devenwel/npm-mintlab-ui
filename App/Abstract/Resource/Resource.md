# 🔌 `Resource` component

> Declarative resource component.

- [API documentation](https://zaaksysteem.gitlab.io/npm-mintlab-ui/documentation/identifiers.html#abstract-resource)

## Usage

- provide a *resource ID* or an array of *resource ID*s
- wrap with a component that connects the `payload` data 
  and the `resolve` method to a store
- passes the resolved data with a `resource` property 
  to the child component

## Dependencies

- `./Clone`
- `../Zaaksysteem/Loader`
