import React from 'react';
import Resource from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `Resource` component', () => {

  test('needs tests', () => {
    const actual = (
      <Resource/>
    );
    const expected = renderToStaticMarkup('');

    expect(actual).toBe(expected);
  });

});
