import { parseSize } from './parseSize';

/**
 * {@link Svg} utility function.
 * @private
 * @param viewBox
 * @param width
 * @return {*}
 */
export function getStyleFromWidth (viewBox, width) {
  const [viewBoxWidth, viewBoxHeight] = viewBox;
  const [size, unit] = parseSize(width);
  const vertical = ((viewBoxHeight / viewBoxWidth) * size);
  const value = `${vertical}${unit}`;

  if (unit === '%') {
    return {
      width,
      paddingTop: value,
    };
  }

  return {
    width,
    height: value,
  };
}
