const sizeExpression = /^(\d+|(?:\d*\.\d+))(%|em|ex|px|rem|vh|vw)$/;

/**
 * {@link Svg} utility function.
 * @private
 * @param {string} value
 *   CSS length or percentage
 * @return {Array<number,string>}
 *   Size/unit tuple
 */
export function parseSize(value) {
  const type = typeof value;

  if (type === 'string') {
    const match = sizeExpression.exec(value);

    if (match) {
      const [, numeric, unit] = match;

      return [Number(numeric), unit];
    }
  }

  throw new TypeError('expected a string');
}
