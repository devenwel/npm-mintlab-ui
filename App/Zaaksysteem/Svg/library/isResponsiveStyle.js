/**
 * {@link Svg} utility function.
 * @private
 * @param style
 * @return {*|boolean}
 */
export const isResponsiveStyle = style => (
  style
  && (
    style.hasOwnProperty('paddingLeft')
    || style.hasOwnProperty('paddingTop')
  )
);
