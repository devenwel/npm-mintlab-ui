import React from 'react';
import Svg from '.';
import { renderToStaticMarkup } from 'react-dom/server';

/**
 * @test {Svg}
 */
describe('The `Svg` component', () => {

  test('creates the expected HTML', () => {
    const actual = renderToStaticMarkup(<Svg viewBox={1}><rect/></Svg>);
    const expected = '<span class="" style="width:1px;height:1px"><svg viewBox="0 0 1 1"><rect></rect></svg></span>';

    expect(actual).toBe(expected);
  });

});
