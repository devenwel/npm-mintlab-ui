import React, { Component } from 'react';
import Render from '../../Abstract/Render/Render';
import { bind } from '../../library';
import styles from './Overlay.css';

const { assign } = Object;

/**
 * Overlay component. Depends on {@link Render}.
 *
 * @see https://zaaksysteem.gitlab.io/npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Overlay
 *
 * @reactProps {boolean} active
 *   Overlay state
 * @reactProps {*} children
 *   Overlay content nodes
 * @reactProps {boolean} [closeOnClick]
 *   Overlay closes on click
 * @reactProps {Function} [onClick]
 *   Overlay click handler
 */
export default class Overlay extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      closeOnClick: true,
    };
  }

  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    bind(this, 'close');
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const { active, children } = this.props;

    return (
      <Render
        condition={active}
      >
        <div
          ref={domNode => assign(this, { domNode })}
          className={styles.overlay}
          onClick={this.close}
        >{children}</div>
      </Render>
    );
  }

  /**
   * @param {Event} event
   */
  close(event) {
    const { closeOnClick } = this.props;

    if (closeOnClick) {
      const { target } = event;

      if (target === this.domNode) {
        const { onClick } = this.props;

        onClick(event);
      }
    }
  }
}
