import React from 'react';
import Overlay from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `Overlay` component', () => {

  test('needs tests', () => {
    const actual = (
      <Overlay/>
    );
    const expected = renderToStaticMarkup('');

    expect(actual).toBe(expected);
  });

});
