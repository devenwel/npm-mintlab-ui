import {
  React,
  stories,
  boolean,
  toggleFactory,
  UseTheKnobs,
} from '../../story';
import Overlay from '.';
import Render from '../../Abstract/Render/Render';

const ID = 'Active';
const close = toggleFactory(Overlay, ID);

stories(module, __dirname, {
  Default() {
    return (
      <div>
        <UseTheKnobs/>
        <Overlay
          active={boolean(ID, true)}
          closeOnClick={boolean('Close on click', true)}
          onClick={close}
        >
          <Render condition={boolean('Message', false)}>
            <div style={{
              padding: '30px 50px',
              background: '#fff',
            }}>
              <p>Hello, world!</p>
              <div>
                <button
                  onClick={close}
                  type="button"
                >Close</button></div>
            </div>
          </Render>
        </Overlay>
      </div>
    );
  },
});
