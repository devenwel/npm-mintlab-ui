import {
  React,
  stories,
  text,
  action,
} from '../../story';
import IconLink from '.';

stories(module, __dirname, {
  Default() {
    return (
      <IconLink
        action={action('IconLink')}
        icon="home"
      >
        {text('Greeting', 'Hello, world!')}
      </IconLink>
    );
  },
  Active() {
    return (
      <IconLink
        action={action('IconLink')}
        active={true}
        icon="home"
      >
        {text('Greeting', 'Hello, world!')}
      </IconLink>
    );
  },
});
