import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Button from '../../Material/Button';
import { Body2 } from '../../Material/Typography';
import { styles } from './style/styles';

/**
 * @param {Object} props
 * @param {Function} props.action
 * @param {boolean} props.active
 * @param {*} props.children
 * @param {Object} props.classes
 * @param {string} props.icon
 * @return {ReactElement}
 */
const IconLink = ({
  action,
  active = false,
  children,
  classes,
  icon,
}) => (
  <span
    className={classNames(classes.root, {
      [classes.active]: active,
    })}
    onClick={action}
  >
    <Button
      overrides={active ? {
        root: classes.shadow,
      } : null}
      presets={['inherit', 'icon']}
    >{icon}</Button>
    <span
      className={classes.label}
    >
      <Body2>{children}</Body2>
    </span>
  </span>
);

export default withStyles(styles, {
  withTheme: true,
})(IconLink);
