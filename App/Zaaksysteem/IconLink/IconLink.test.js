import React from 'react';
import IconLink from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `IconLink` component', () => {

  test('creates the expected HTML', () => {
    const actual = renderToStaticMarkup(<IconLink>TEST</IconLink>);
    const expected = '<div>TEST</div>';

    expect(actual).toBe(expected);
  });

});
