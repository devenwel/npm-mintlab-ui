export const styles = theme => ({
  root: {
    cursor: 'pointer',
    display: 'flex',
    alignItems: 'center',
  },
  active: {
    color: theme.palette.primary.main,
  },
  label: {
    marginLeft: '27px',
  },
  shadow: {
    boxShadow: '0 2px 3px #ccc',
  },
});
