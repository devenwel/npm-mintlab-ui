import {
  React,
  stories,
  event,
  boolean,
  text,
} from '../../story';
import Action from '.';

stories(module, __dirname, {
  Default() {
    return (
      <Action
        active={boolean('Active', true)}
        onClick={event(Action)}
      >
        {text('Greeting', 'Hello, world!')}
      </Action>
    );
  },
});
