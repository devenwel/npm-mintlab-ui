import React from 'react';
import Action from '.';
import { renderToStaticMarkup } from 'react-dom/server';

/**
 * @test {Action}
 */
describe('The `Action` component', () => {

  test('creates the expected HTML', () => {
    const actual = renderToStaticMarkup(<Action>TEST</Action>);
    const expected = '<button class="" type="button">TEST</button>';

    expect(actual).toBe(expected);
  });

});
