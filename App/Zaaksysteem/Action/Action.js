import React from 'react';
import classNames from 'classnames';
import style from './Action.css';

/**
 * Action component.
 *
 * @see https://zaaksysteem.gitlab.io/npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Action
 *
 * @param {Object} props
 * @param {boolean} [props.active=true]
 *   Action state
 * @param {*} props.children
 *   Action content nodes
 * @param {string} [props.className]
 *   CSS class
 * @param {Function} [props.onClick=null]
 *   Click handler
 * @return {ReactElement}
 */
const Action = ({
  active = true,
  children,
  className,
  onClick,
}) => (
  <button
    className={classNames(style.action, className)}
    disabled={!active}
    onClick={active ? onClick : null}
    type="button"
  >
    {children}
  </button>
);

export default Action;
