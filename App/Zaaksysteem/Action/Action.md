# 🔌 `Action` component

> Generic action component for actionable items that are not 
  [Button](https://zaaksysteem.gitlab.io/npm-mintlab-ui/storybook/?selectedKind=Material/Button)s.

- [API documentation](https://zaaksysteem.gitlab.io/npm-mintlab-ui/documentation/identifiers.html#zaaksysteem-action)
