# 🔌 `Loader` component

> React loader component based on 
  [SpinKit](https://github.com/tobiasahlin/SpinKit)
  with configurable properties.

- [API documentation](https://zaaksysteem.gitlab.io/npm-mintlab-ui/documentation/identifiers.html#zaaksysteem-loader)
