import Loader from '.';
import {
  React,
  stories,
  boolean,
  select,
} from '../../story';

stories(module, __dirname, {
  Default() {
    return (
      <Loader
        active={boolean('Active', true)}
        color={select('Color', {
          '#000': 'Black',
          '#f00': 'Red',
        }, '#000')}
        type={select('Type', {
          circle: 'Circle',
          cube: 'Cube',
          fold: 'Fold',
          pulse: 'Pulse',
          wave: 'Wave',
        }, 'circle')}
      >Loader content</Loader>
    );
  },
});
