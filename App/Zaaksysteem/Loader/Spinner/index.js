/**
 * Dictionary for mapping a string `prop` to a component.
 */
export { Circle as circle } from './Circle';
export { Cube as cube } from './Cube';
export { Fold as fold } from './Fold';
export { Pulse as pulse } from './Pulse';
export { Wave as wave } from './Wave';
