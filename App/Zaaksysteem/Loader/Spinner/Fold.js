import React from 'react';
import { iterator } from '../library/iterator';
import style from './Fold.css';

const LENGTH = 4;

/**
 * Spinner type for the {@link Loader} component
 * @param {Object} props
 * @param {string} props.color
 * @return {ReactElement}
 */
export const Fold = ({ color }) => (
  <div className={style['fold']}>
    {iterator(LENGTH).map(item => (
      <div
        key={item}
        className={style['cube']}
      >
        <span
          className={style['square']}
          style={{
            backgroundColor: color,
          }}
        />
      </div>
    ))}
  </div>
);
