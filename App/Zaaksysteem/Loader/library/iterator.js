const INCREMENT = 1;

/**
 * {@link Loader} component utility function.
 * @private
 * @param {number} length
 * @return {Array<number>}
 */
export const iterator = length =>
  Array
    .from(Array(length).keys())
    .map(number => number + INCREMENT);
