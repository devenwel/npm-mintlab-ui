import React from 'react';
import Loader from '.';
import { renderToStaticMarkup } from 'react-dom/server';

/**
 * @test {Loader}
 */
describe('The `Loader` component', () => {

  test('creates the expected HTML', () => {
    const actual = renderToStaticMarkup(
      <Loader
        active={false}
      >TEST</Loader>
    );
    const expected = 'TEST';

    expect(actual).toBe(expected);
  });

});
