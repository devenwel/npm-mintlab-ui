import { createElement } from 'react';
import * as map from './Spinner';

/**
 * @see https://zaaksysteem.gitlab.io/npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Loader
 *
 * @param {Object} props
 * @param {boolean} [props.active=true]
 * @param {ReactElement} props.children
 * @param {string} [props.color='#000']
 *   CSS color
 * @param {string} [props.type='circle']
 *   Loader name; available loaders are
 *   - `'circle'` ({@link Circle})
 *   - `'cube'` ({@link Cube})
 *   - `'fold'` ({@link Fold})
 *   - `'pulse'` ({@link Pulse})
 *   - `'wave'` ({@link Wave})
 * @return {ReactElement}
 */
const Loader = ({
  active = true,
  children,
  color = '#000',
  type = 'circle',
}) => {
  if (active) {
    return createElement(map[type], {
      color,
    });
  }

  return children;
};

export default Loader;
