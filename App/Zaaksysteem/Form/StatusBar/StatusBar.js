import React from 'react';
import Icon from '../../../Material/Icon';
import { Body1 } from '../../../Material/Typography/Typography';
import { withStyles } from '@material-ui/core/styles';
import styles from './style/styles';
import Button from '../../../Material/Button';
import Card from '../../../Material/Card';
import classNames from 'classnames';
import Render from '../../../Abstract/Render';

const MSG_ERRORS = 'Er zitten fouten in één of meerdere configuratie-items.';
const MSG_UNSAVED = 'Er zijn onopgeslagen configuratie-items.';
const MSG_SAVE = 'Opslaan';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} props.save
 * One or more errors present
 * @param {boolean} [props.disabled=false]
 * Form value(s) have been changed, but not yet saved
 * @param {boolean} [props.changed=false]
 * @param {Object} props.translations
 * @return {ReactElement}
 */
const StatusBar = ({
  classes,
  save,
  disabled=false,
  changed=false,
  translations={
    ['form:errorsInForm']: MSG_ERRORS,
    ['form:unsavedChanges']: MSG_UNSAVED,
    ['form:save']: MSG_SAVE,
  },
}) => (
  <Card
    classes={{
      root: classNames(classes.bar, {
        [classes.disabled]: disabled,
      }),
    }}
    contentClasses={{
      root: classes.content,
    }}
  >
    <Render condition={disabled || changed}>
      <Icon classes={{
        root: classes.icon,
      }}>help_outline</Icon>
    </Render>

    <Render condition={disabled}>
      <Body1>{translations['form:errorsInForm']}</Body1>
    </Render>

    <Render condition={!disabled && changed}>
      <Body1>{translations['form:unsavedChanges']}</Body1>
    </Render>

    <Button
      disabled={disabled || !changed}
      action={save}
      presets={['raised', 'primary']}
      classes={{
        raised: classes.save,
      }}>
      {translations['form:save']}
    </Button>

  </Card>
);

export default withStyles(styles)(StatusBar);
