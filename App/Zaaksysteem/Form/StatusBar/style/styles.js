/**
 * @param {Object} theme
 * @return {Object}
 */
const styles = ({
  palette: {
    text,
    error,
  },
  zIndex,
}) => ({
  bar: {
    //ZS-TODO: position the statusbar fixed over the full width of the content area
    position: 'fixed',
    right: '30px',
    width: '600px',
    borderRadius: '0px',
    zIndex: zIndex.statusBar,
  },
  disabled: {
    backgroundColor: error.light,
  },
  save: {
    marginLeft: 'auto',
  },
  icon: {
    marginRight: '10px',
    color: text.primary,
    fontSize: '20px',
  },
  content: {
    display: 'flex',
    'align-items': 'center',
    '&:last-child': {
      padding: '12px 24px 12px 24px',
    },
  },
});

export default styles;
