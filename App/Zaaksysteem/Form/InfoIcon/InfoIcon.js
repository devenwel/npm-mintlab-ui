import React from 'react';
import Icon from '../../../Material/Icon';
import { withStyles } from '@material-ui/core/styles';
import styles from './style/styles';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @return {ReactElement}
 */
const InfoIcon = ({
  classes,
}) => (
  <Icon
    classes={classes}
  >help_outline</Icon>
);

export default withStyles(styles)(InfoIcon);
