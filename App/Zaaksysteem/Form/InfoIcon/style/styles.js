/**
 * @param {Object} theme
 * @return {Object}
 */

const GREY_WEIGHT = 400;

const styles = ({
  palette: {
    primary,
    grey,
  },
}) => ({
  root: {
    color: grey[GREY_WEIGHT],
    fontSize: '20px',
    '&:hover': {
      color: primary.main,
    },
  },
});

export default styles;
