import React from 'react';
import Form from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `Form` component', () => {

  test('needs tests', () => {
    const actual = (
      <Form/>
    );
    const expected = renderToStaticMarkup('');

    expect(actual).toBe(expected);
  });

});
