import React, { Component, createElement, Fragment } from 'react';
import Card from '../../Material/Card';
import Render from '../../Abstract/Render';
import * as typeMap from './library/typemap';
import TooltipWrapper from './TooltipWrapper/TooltipWrapper';
import InfoIcon from './InfoIcon/InfoIcon';
import StatusBar from './StatusBar/StatusBar';
import { bind, equals } from '../../library';
import { withStyles } from '@material-ui/core/styles';
import styles from './style/styles';

const { assign, keys } = Object;

/**
 * @see  https://zaaksysteem.gitlab.io/npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Form
 *
 * @reactProps {Array<Object>} fieldSets
 *   Form control configurations, grouped, with an optional title and description
  * @reactProps {Function} save
 *   Save the external state manager's model values
 * @reactProps {Function} set
 *   Set a form control's model value in the external state manager
 * @reactProps {Function} validate
 *   Validate a form control value literal
 * @reactProps {string} identifier
 *   String used to generate a unique key for every form field
 */
class Form extends Component {
  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);

    this.state = this.getStateFromFields();

    this.setExternalState();
    bind(this, 'onBlur', 'onFocus', 'onChange');
  }

  // Lifecycle methods:

  componentDidUpdate(prevProps) {
    if (!equals(prevProps.fieldSets, this.props.fieldSets)) {
      this.setState(this.getStateFromFields());
      this.setExternalState();
    }
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      getFieldSet,
      onBlur,
      onFocus,
      onChange,
      props: {
        classes,
        fieldSets,
        save,
        changed,
      },
      state: {
        disabled,
      },
    } = this;

    return (
      <Fragment>
        <StatusBar
          save={save}
          disabled={disabled}
          changed={changed}
        />

        <div
          onBlur={onBlur}
          onChange={onChange}
          onFocus={onFocus}
          className={classes.form}
        >
          { fieldSets && fieldSets.map(getFieldSet, this) }
        </div>
      </Fragment>
    );
  }

  // Custom methods:

  /**
   * Get a flattened array of all fields in the fieldset
   *
   * @param {Object} fieldSet
   * @return {Array}
   */
  getFieldList(fieldSet) {
    return fieldSet ? fieldSet.reduce((accumulator, { fields }) => accumulator.concat(fields), []) : [];
  }

  /**
   * Get the formatted literal value if it is valid and
   * not the empty string, otherwise `undefined`.
   *
   * @param {string} name
   * @param {*} value
   * @return {string|undefined}
   */
  /* eslint complexity: [2, 10] */
  getModelValue(name, value) {
    const { errors } = this.state;

    if (errors.hasOwnProperty(name) || value === '') {
      return;
    }

    return value;
  }

  getFieldByName(needle) {
    const { fieldSets } = this.props;
    const findField = ({ name }) => name === needle;

    return this.getFieldList(fieldSets).find(findField);
  }

  getNormalizedFormValue({ checked, type, value, constraints }) {
    if (type === 'checkbox') {
      return checked;
    }

    if (constraints && constraints.includes('number')) {
      return Number(value);
    }

    return value;
  }

  /**
   * Validate the value whenever it changes and update
   * its error status in the internal component state.
   * As long as any form error remains, submission is disabled.
   *
   * @param {Object} event
   * @param {Object} event.target
   * @param {string} event.target.name
   * @param {string} event.target.value
   */
  onChange({
    target: {
      checked,
      name,
      type,
      value,
    },
  }) {
    const {
      props: {
        validate,
      },
      state: {
        errors,
      },
    } = this;

    const field = this.getFieldByName(name);
    if (!field) return;

    const {
      constraints,
      config,
    } = field;

    const normalizedValue = this.getNormalizedFormValue({
      checked,
      type,
      value,
      constraints,
      config,
    });

    const validity = validate(normalizedValue, constraints, config);
    const nextErrors = assign({}, errors);

    if (validity) {
      nextErrors[name] = validity;
    } else {
      delete nextErrors[name];
    }

    this.setState({
      disabled: Boolean(keys(nextErrors).length),
      errors: nextErrors,
    });

  }

  /**
   * When the control loses focus, delegate its
   * key/value pair to the external state manager.
   * If the actual value is not valid, it is set to `undefined`.
   *
   * @param {Object} event
   * @param {Object} event.target
   * @param {string} event.target.name
   * @param {string} event.target.value
   */
  onBlur({
    target: {
      checked,
      name,
      type,
      value,
    },
  }) {
    const {
      props: {
        set,
        validate,
      },
    } = this;

    const field = this.getFieldByName(name);
    if (!field) return;

    this.setState({
      focus: undefined,
    });

    const {
      constraints,
      config,
    } = field;

    const normalizedValue = this.getNormalizedFormValue({
      checked,
      type,
      value,
      constraints,
    });
    const error = validate(normalizedValue, constraints, config);

    if (error) {
      this.setState({
        errors: assign({}, this.state.errors, {
          [name]: error,
        }),
      });

      // ZS-FIXME: current only match is resetting boolean/checkbox
      // needs back-end input
      set({
        [name]: undefined,
      });
    } else {
      const nextValue = this.getModelValue(name, normalizedValue);

      set({
        [name]: nextValue,
      });
    }
  }

  /**
   * @param {Object} event 
   * @param {Object} event.target
   * @param {string} event.target.name
   */
  onFocus({ target: { name }} ) {
    this.setState({ 
      focus: name,
    });
  }

  getStateFromFields() {
    const { fieldSets, validate } = this.props;

    const reduceErrors = (accumulator, { constraints, name, value, config }) => {
      const error = validate(value, constraints, config);

      if (error) {
        accumulator[name] = error;
      }

      return accumulator;
    };

    const errors = this.getFieldList(fieldSets).reduce(reduceErrors, {});

    // ZS-TODO: introduce a method
    const disabled = Boolean(keys(errors).length);

    return {
      disabled,
      errors,
    };
  }

  setExternalState() {
    const { fieldSets, set } = this.props;

    for (const { name, value } of this.getFieldList(fieldSets)) {
      set({
        [name]: this.getModelValue(name, value),
      });
    }
  }

  /**
   * @param {Object} fieldSet
   * @param {*} index
   * @return {ReactElement}
   */
  getFieldSet({
    title,
    description,
    fields,
  }, index) {

    const {
      props: {
        identifier,
      },
      getFormControl,
    } = this;

    return (
      <div
        key={`${identifier}-${index}`}
      >

        <Render condition={title && fields.length}>
          <Card
            title={title}
            description={description}
          >
            {fields.map(getFormControl, this)}
          </Card>
        </Render>

        <Render condition={ !title }>
          {fields.map(getFormControl, this)}
        </Render>

      </div>
    );
  }

  /**
   * @param {Object} props
   * @param {string} [props.constraints=[]]
   * @param {string} props.name
   * @param {string} props.type
   * @param {string} [props.value]
   * @return {ReactElement}
   */
  getFormControl({
    constraints=[],
    name,
    type,
    value,
    ...rest
  }) {
    const {
      state: {
        errors,
        focus,
      },
      props: {
        identifier,
        classes,
      },
      onChange,
      onBlur,
      onFocus,
    } = this;

    const error = errors[name];
    const required = constraints.includes('required');
    const hasFocus = focus === name;

    // ZS-TODO: connect infoIcon to translations, only render if there is a tooltip for this field
    return (
      <div
        key={`${identifier}-${name}`}
        className={classes.formRow}
      >

        <TooltipWrapper
          condition={ Boolean(error && !hasFocus) }
          title={error}
        >
          {createElement(typeMap[type], {
            // ZS-FIXME: add generic normalization
            defaultChecked: (typeof value === 'boolean') ? value : undefined,
            error,
            name,
            onFocus,
            onBlur,
            onChange,
            required,
            value,
            ...rest,
          })}
        </TooltipWrapper>
        
        <Render condition={true}>
          <TooltipWrapper 
            title={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pellentesque aliquam magna eu ullamcorper. '}
            type='info'
          >
            <InfoIcon />
          </TooltipWrapper>
        </Render>

      </div>
    );
  }
}

export default withStyles(styles)(Form);
