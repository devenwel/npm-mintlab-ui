/**
 * @param {Object} theme
 * @return {Object}
 */
const styles = ({
  breakpoints,
}) => ({
  form: {
    width: '100%',
    marginTop: '80px',
    marginBottom: '80px',
  },
  formRow: {
    marginBottom: '16px',
    display: 'grid',
    'grid-template-columns': 'auto 30px',
    'grid-column-gap': '20px',
    'align-items': 'center',
    [breakpoints.up('md')]: {
      'grid-template-columns': '50% 30px',
    },
  },
});

export default styles;
