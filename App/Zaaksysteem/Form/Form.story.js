import {
  React,
  stories,
  action,
  boolean,
} from '../../story';
import Form from '.';

const getTextContent = value => {
  const proxy = document.createElement('DIV');

  proxy.innerHTML = value;

  return proxy.textContent;
};

const isEmptyString = value =>
  (value.trim() === '');

const isNumber = value => (
  Number.isFinite(value)
  && !Number.isNaN(value)
);

const isInteger = value =>
  /^-?\d+$/.test(value);

const constraintsMap = {
  required(userInput) {
    switch (typeof userInput) {
    case 'string': {
      const textContent = getTextContent(userInput);

      return !isEmptyString(textContent);
    }
    default:
      return Boolean(userInput);
    }
  },
  number(userInput, { type } = {}) {
    if (!isNumber(userInput)) {
      return false;
    }

    if (type === 'integer') {
      return isInteger(userInput);
    }

    return true;
  },
};

const validate = (value, constraints = [], config) => {
  for (const constraint of constraints) {
    if (!constraintsMap[constraint](value, config)) {
      return constraint;
    }
  }
};

const fieldSets = [
  {
    title: 'test1',
    description: 'description 1',
    fields: [
      {
        choices: [
          { value: 'chocolate', label: 'Chocolate' },
          { value: 'strawberry', label: 'Strawberry' },
          { value: 'vanilla', label: 'Vanilla' },
        ],
        constraints: ['required'],
        label: 'Select test',
        name: 'selecttest',
        type: 'select',
        value: undefined,
      },
      {
        type: 'text',
        config: {},
        label: 'Test label 1',
        name: 'test1',
        isMulti: false,
        disabled: false,
        reference: '1',
        required: false,
        value: '',
        constraints: ['required'],
      },
      {
        constraints: ['required'],
        label: 'HTML editor',
        name: 'htmltest',
        type: 'html',
        value: 'test <strong>value</strong> in story',
      },
    ],
  },
];

const stores = {
  Default: {},
};

stories(module, __dirname, {
  Default({ store }) {
    const set = keyValuePairs => {
      store.set(keyValuePairs);
    };

    return (
      <Form
        fieldSets={fieldSets}
        save={action('SAVE')}
        set={set}
        validate={validate}
        changed={boolean('Unsaved changes', false)}
      />
    );
  },
}, stores);
