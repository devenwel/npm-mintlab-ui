/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the ErrorLabel component
 * @param {Object} theme
 * @return {Object}
 */
const styles = ({
  palette: {
    error,
  },
}) => ({
  root: {
    color: error.main,
  },
});

export default styles;
