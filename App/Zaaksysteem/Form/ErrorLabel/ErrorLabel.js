import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Caption } from '../../../Material/Typography/Typography';
import styles from './style/styles';

/**
 * @param {Object} props
 * @param {string} props.label
 * @param {Object} props.classes
 * @return {ReactElement}
 */
const ErrorLabel = ({
  label,
  classes,
}) =>(
  <Caption classes={classes}>
    { label }
  </Caption>
);

export default withStyles(styles)(ErrorLabel);
