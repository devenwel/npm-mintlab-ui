# `Form` component

## Abstract

- render **uncontrolled** fields from `prop`
    - on construction
        - validate all field values
        - save all valid values to external store
- event delegation
    - on `input`
        - validate the `target` value
        - disable/enable the form
    - on `blur`
        - save valid target value to external store
- submit
    - send external store values to permanent data storage
