# 🔌 `Logo` component

> Zaaksysteem logo component.

- [API documentation](https://zaaksysteem.gitlab.io/npm-mintlab-ui/documentation/identifiers.html#zaaksysteem-logo)
