import {
  React,
  stories,
  boolean,
} from '../../story';
import Logo from '.';

stories(module, __dirname, {
  Default() {
    return (
      <div>
        <Logo
          width="500px"
          verbose={boolean('Verbose', true)}
        />
      </div>
    );
  },
});
