import React from 'react';
import Logo from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `Logo` component', () => {

  test('needs tests', () => {
    const actual = (
      <Logo/>
    );
    const expected = renderToStaticMarkup('');

    expect(actual).toBe(expected);
  });

});
