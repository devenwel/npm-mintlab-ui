import React from 'react';
import Layout from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `Layout` component', () => {

  test('creates the expected HTML', () => {
    const actual = renderToStaticMarkup(<Layout>TEST</Layout>);
    const expected = '<div>TEST</div>';

    expect(actual).toBe(expected);
  });

});
