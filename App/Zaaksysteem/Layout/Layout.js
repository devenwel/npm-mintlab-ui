import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import AppBar from '../../Material/AppBar';
import Drawer from '../../Material/Drawer';
import IconLink from '../IconLink';
import { styles } from './style/styles';

const DEFAULT_INDEX = 0;

/**
 * @param {Object} props
 * @param {Array} props.children
 * @return {ReactElement}
 */
const Layout = ({
  active = DEFAULT_INDEX,
  appBar,
  children,
  classes,
  drawer,
  isDrawerOpen,
  onUserClick,
  toggleDrawer,
}) => (
  <div
    className={classes.root}
  >
    <AppBar
      className={classNames(classes.appBar)}
      gutter={12}
      onMenuClick={toggleDrawer}
      onUserClick={onUserClick}
      position="absolute"
    >{appBar}</AppBar>
    <Drawer
      variant="permanent"
      classes={{
        paper: classNames(classes.drawerPaper, {
          [classes.drawerPaperClose]: !isDrawerOpen,
        }),
      }}
      open={isDrawerOpen}
    >
      <div
        className={classes.toolbar}
      />
      <nav>
        <ul
          className={classes.list}
        >
          {drawer.map(({ action, icon, label }, index) => (
            <li
              key={index}
              className={classes.item}
            >
              <IconLink
                action={() => action(index)}
                active={(index === active)}
                icon={icon}
              >{label}</IconLink>
            </li>
          ))}
        </ul>
      </nav>
    </Drawer>
    <div
      className={classes.contentWrapper}
    >
      <div
        className={classes.toolbar}
      />
      <main
        className={classes.content}
      >{children}</main>
    </div>
  </div>
);

export default withStyles(styles, {
  withTheme: true,
})(Layout);
