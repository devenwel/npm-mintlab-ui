import {
  React,
  stories,
  boolean,
  text,
} from '../../story';
import Editor from '.';

stories(module, __dirname, {
  Default() {
    return (
      <Editor
        name='editor'
        value={'<p>Hey this <strong>editor</strong> rocks 😀</p>'}
        readOnly={boolean('Read-only', false)}
        error={text('Error', '')}
      />
    );
  },
});
