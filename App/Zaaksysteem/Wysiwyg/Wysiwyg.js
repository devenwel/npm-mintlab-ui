import React, { Component } from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, ContentState, convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import Render from '../../Abstract/Render';
import { bind } from '../../library';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import ErrorLabel from '../Form/ErrorLabel/ErrorLabel';
import styles from './style/styles';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

/**
 * Wrapper component for `react-draft-wysiwyg`.
 * Additional props will be passed through to that component.
 *
 * @see https://jpuri.github.io/react-draft-wysiwyg/#/docs
 * @see https://zaaksysteem.gitlab.io/npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Wysiwyg
 *
 * @reactProps {boolean} disabled
 * @reactProps {string} [error]
 * @reactProps {string} name
 * @reactProps {Function} [onChange]
 * @reactProps {Function} [onBlur]
 * @reactProps {string} [value='']
 * 
 */
class Wysiwyg extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      toolbar: {
        options: ['inline'],
      },
      value: '',
      disabled: false,
    };
  }

  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);

    const contentBlock = htmlToDraft(props.value);
    this.state = {
      hasFocus: false,
    };

    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      const editorState = EditorState.createWithContent(contentState);

      this.state.editorState = editorState;
    }

    bind(this, 'handleBlur', 'handleChange', 'handleFocus');
  }

  /**
   * @return {ReactElement}
   */
  /* eslint-disable no-unused-vars */
  render() {
    const {
      props: {
        error,
        name,
        onBlur,
        onChange,
        onFocus,
        value,
        classes: {
          defaultClass,
          errorClass,
          editorClass,
        },
        disabled,
        ...rest
      },
      state: {
        editorState,
        hasFocus,
      },
      handleBlur,
      handleChange,
      handleFocus,
    } = this;

    return (
      <div>
        <Editor
          defaultEditorState={editorState}
          onEditorStateChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          wrapperClassName={ classNames(defaultClass, {[errorClass]: error}) }
          toolbarClassName={ classNames(defaultClass, {[errorClass]: error}) }
          editorClassName={ classNames(defaultClass, editorClass, {[errorClass]: error}) }
          readOnly={disabled}
          {...rest}
        />
        <Render condition={ Boolean(hasFocus && error) }>
          <ErrorLabel label={error} />
        </Render>
      </div>
    );
  }

  handleChange(editorState) {
    const { name, onChange } = this.props;

    this.setState({
      editorState,
    });

    if (!onChange) return;

    const value = draftToHtml(convertToRaw(editorState.getCurrentContent()));

    onChange({
      target: {
        name,
        type: 'html',
        value,
      },
    });
  }

  /**
   * @param {Event} event 
   */
  handleBlur(event) {
    const { onBlur, name } = this.props;
    event.stopPropagation();

    this.setState({
      hasFocus: false,
    });

    if (!onBlur) return;

    const value = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));

    onBlur({
      target: {
        name,
        type: 'html',
        value,
      },
    });
  }

  /**
   * @param {Event} event 
   */
  handleFocus(event) {
    const { onFocus, name } = this.props;
    event.stopPropagation();

    this.setState({
      hasFocus: true,
    });

    if (!onFocus) return;

    onFocus({
      target: {
        name,
      },
    });
  }
}

export default withStyles(styles)(Wysiwyg);
