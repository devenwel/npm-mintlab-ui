# `Wysiwyg` component

> HTML editor, uses `React Draft Wysiwyg` 

Pass in name and value props, and any configuration props from react-draft-wysiwyg.

- API documentation [https://jpuri.github.io/react-draft-wysiwyg/#/docs]

## Example
    <Wysiwyg 
      name='test'
      value='test value'
      wrapperClassName='wrapper-class'
      toolbar={{
        options: ['inline', 'fontSize']
      }}
    >

