/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the Wywisyg component
 * @param {Object} theme
 * @return {Object}
 */
const styles = ({ 
  palette: {
    error,
    grey,
  },
  shape: {
    borderRadius,
  },
  typography: {
    fontFamily,
  },
}) => ({
  defaultClass: {
    backgroundColor: grey[grey.defaultWeight],
    '& .rdw-option-wrapper': {
      backgroundColor: grey[grey.defaultWeight],
    },
    borderRadius,
    fontFamily,

  },
  errorClass: {
    backgroundColor: error.light,
    '& .rdw-option-wrapper': {
      backgroundColor: error.light,
    },
  },
  editorClass: {
    padding: '10px',
  },
});

export default styles;
