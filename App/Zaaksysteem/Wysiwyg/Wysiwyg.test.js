import React from 'react';
import Wysiwyg from '.';
import { renderToStaticMarkup } from 'react-dom/server';

xdescribe('The `Wysiwyg` component', () => {

  test('needs tests', () => {
    const actual = (
      <Wysiwyg/>
    );
    const expected = renderToStaticMarkup('');

    expect(actual).toBe(expected);
  });

});
