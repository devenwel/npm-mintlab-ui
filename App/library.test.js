import { 
  cloneWithout, 
  equals,
  traverseMap,
} from './library';

describe('The library module', () => {
  describe('exports a `cloneWithout` function', () => {
    test('that can clone an object with one excluded property', () => {
      const original = {
        foo: 'foo',
        bar: 'bar',
      };
      const actual = cloneWithout(original, 'foo');
      const expected = {
        bar: 'bar',
      };

      expect(actual).toEqual(expected);
    });

    test('that can clone an object with multiple excluded properties', () => {
      const original = {
        foo: 'foo',
        bar: 'bar',
        quux: 'quux',
      };
      const actual = cloneWithout(original, 'foo', 'quux');
      const expected = {
        bar: 'bar',
      };

      expect(actual).toEqual(expected);
    });
  });

  describe('exports an `equals` function', () => {
    test('that deeply compares values that can be serialized as JSON', () => {
      const actual = {
        foo: 'bar',
        baz: 42,
      };
      const expected = {
        foo: 'bar',
        baz: 42,
      };

      expect(equals(actual, expected)).toBe(true);
    });
  });

  describe('exports a `traverseMap` function', () => {
    test('that returns the output of first value function where the key function is truthy', () => {
      const map = new Map([
        [() => false, () => false],
        [(first, second) => second === 'b', (third, fourth) => fourth],
      ]);
      
      const keyArgs = ['a', 'b'];
      const functionArgs = ['c', 'd'];
      const actual = traverseMap(map, keyArgs, functionArgs);
      const [, expected] = functionArgs;

      expect(equals(actual, expected)).toBe(true);
    });
  });

});
