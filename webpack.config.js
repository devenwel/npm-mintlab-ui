const requireDirectory = require('require-directory');
require('./webpack/library/entry');

module.exports = requireDirectory(module, './webpack/config');
