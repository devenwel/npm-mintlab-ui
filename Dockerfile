FROM node:carbon-alpine
RUN npm install -g npm@6.1.0 && \
    mkdir -p        /home/node/ui/node_modules /home/node/ui/test/coverage && \
    chown node:node /home/node/ui/node_modules /home/node/ui/test/coverage
USER node
CMD ["/bin/sh", "--login"]
