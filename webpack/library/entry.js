/**
 * Generate the main file of the published package.
 * All public modules are named exports.
 * ZS-TODO: use named exports in the implementations as well
 */
const { writeFileSync } = require('fs');
const glob = require('glob');
const { parse } = require('path');

const APP = './App';
const GLOB = './*/*/!(*.story|*.test).js';
const BOILERPLATE = '// GENERATED ENTRY POINT FILE FOR THE WEBPACK PUBLICATION BUILD, DO NOT EDIT.';
const JOIN = '\n';
const DIR_SLICE_INDEX = -1;
const exportAll = ['typography', 'exports'];

function getModulePath(filePath) {
  const { dir, name } = parse(filePath);

  if (exportAll.includes(name.toLowerCase())) {
    return `export * from '${dir}';`;
  }

  const [last] = dir
    .split('/')
    .slice(DIR_SLICE_INDEX);

  if (name !== last) {
    return `export {default as ${name}} from '${dir}/${name}';`;
  }

  return `export {default as ${name}} from '${dir}';`;
}

const files = glob
  .sync(GLOB, {
    cwd: APP,
  })
  .map(fileName => getModulePath(fileName));

const output = [
  BOILERPLATE,
  files.join(JOIN),
].join(JOIN);

writeFileSync(`${APP}/index.js`, output);
