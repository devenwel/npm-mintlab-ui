const { join } = require('path');

module.exports = {
  alias: {
    immutable: join(process.cwd(), 'node_modules', 'immutable'),
  },
};
