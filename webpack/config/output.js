const { join } = require('path');

module.exports = {
  filename: 'ui.js',
  libraryTarget: 'commonjs2',
  path: join(process.cwd(), 'distribution'),
};
