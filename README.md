# @mintlab/ui  [![npm version][npm-image]][npm-url]

> Mintlab React UI components used in Zaaksysteem.

## Documentation

- [https://zaaksysteem.gitlab.io/npm-mintlab-ui/](https://zaaksysteem.gitlab.io/npm-mintlab-ui/)

## License

Copyright 2017 Mintlab B.V.

Licensed under the EUPL, Version 1.1 or – as soon they will be approved by the European Commission - 
subsequent versions of the EUPL (the "Licence").

You may not use this work except in compliance with the Licence.

You may obtain a copy of the Licence at: 
[https://joinup.ec.europa.eu/software/page/eupl](https://joinup.ec.europa.eu/software/page/eupl)

[npm-image]: https://img.shields.io/npm/v/@mintlab/ui.svg?style=flat-square
[npm-url]: https://www.npmjs.com/package/@mintlab/ui
