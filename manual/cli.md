# CLI commands

In your host, run

    $ docker-compose exec app sh

## Start the storybook

    $ yarn start

## Generate a component

    $ yarn plop

- Provide the *component name* and choose a 
  *component type* when prompted.

## Run the tests

    $ yarn test

to run the tests once or
    
    $ yarn tdd

to watch the source code for changes during development.

## Lint the JS files

    $ yarn lint

