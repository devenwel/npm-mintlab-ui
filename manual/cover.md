# The `@mintlab/ui` manual

> [Presentational](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0)
  React UI components for Zaaksysteem.

## Audience

## Artefacts

### npm package

[`@mintlab/ui`](https://www.npmjs.com/package/@mintlab/ui)

    $ npm install @mintlab/ui

or

    $ yarn add @mintlab/ui

### GitLab Pages

[http://zaaksysteem.gitlab.io/npm-mintlab-ui/](http://zaaksysteem.gitlab.io/npm-mintlab-ui/)

Covers the generated HTML of

- [Storybook](https://storybook.js.org/)
    - Interactive demo and style guide
- [ESDoc](https://esdoc.org/)
    - API documentation and this manual :-) 
- [Jest](https://facebook.github.io/jest/)
    - Unit test coverage report 
