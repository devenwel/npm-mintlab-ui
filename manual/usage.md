# Usage

## Installation

    $ yarn add @mintlab/ui

## Importing

    import { Resource } from '@mintlab/ui';

### Starting the application

Run

    $ docker-compose up ui

and in another tab

    $ docker-compose exec ui sh

or in a single step (`bash`):
  
    $ ./dev.sh

The first time in the guest you'll want to run

    $ yarn

to install dependencies and then

    $ yarn start

to run storybook.


#### Documentation

- [ESDoc](https://esdoc.org/)

You can generate `./documentation` by running

    $ yarn esdoc
