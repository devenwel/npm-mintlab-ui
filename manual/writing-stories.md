# Writing stories

    import {
      h,
      action,
      // Decorated action to log event type.
      // See Material/Button for an example
      event,
      // knobs:
      array,
      boolean,
      button,
      color,
      date,
      number,
      object,
      select,
      text,
      // Toggle boolean knob from component.
      // See Material/Switch for an example
      toggleFactory,
      // Workaround for recursive toggle.
      // See Material/Drawer for an example
      disableBooleanKnobFactory,
    } from '../../story';
    
    // 'module' and '__dirname' are Node.js alike variables 
    // that are injected into the module by webpack.
    // 'module' is needed for webpack HMR, '__dirname' is used
    // to resolve the component's README file. 
    stories(module, __dirname, {
      FirstStory() {
        return (
          // story component JSX
        );
      },
      SecondStory() {
        return (
          // story component JSX
        );
      },
    });
