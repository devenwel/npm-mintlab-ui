# ▶ Abstract

Components that do not themselves create JSX.
