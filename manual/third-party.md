# Third party dependencies

- [*React*](https://reactjs.org/docs/react-api.html)
- [*Material Components for the Web*](https://material.io/components/web/)

## Configuration and infrastructure

- [Storybook](https://storybook.js.org/basics/introduction/)
- [webpack configuration](https://webpack.js.org/configuration/)
    - cf. `./storybook/webpack`
- [Jest configuration](http://facebook.github.io/jest/docs/en/configuration.html)    
- [PLOP](https://plopjs.com/)
