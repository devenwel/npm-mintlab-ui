# Coding style requirements

- Export every component as `default`. Otherwise the build 
  script will explode.

## Stateless functional component

If you don't need lifecycle methods, always prefer a 
stateless functional component. Using an arrow function
is usually concise and readable.

    const MyComponent = ({ children }) => (
      <div>{children}</div>
    );
    
    export default MyComponent;
    
Sometimes you need to do something based on the passed props
before you `return`. Using a function declaration is also 
fine in that case.

    export default function MyComponent({ children }) {
      // do stuff based on the passed props

      return (
        <div>{children}</div>
      );
    }

## Class component

If you **do** need lifecycle methods, use a class component.
This should only be necessary if your component needs to 
interact with third party code that mangles the DOM itself.

Hint: if the third party code doesn't provide hooks for
async DOM manipulation, try
[MutationObserver](https://dom.spec.whatwg.org/#mutationobserver)s.
Cf. `./App/Snackbar` for an example. Also, bug the maintainers.

    export default class MyComponent {
        componentDidMount() {
          // The DOM provided by (Pr|R)eact is ready,
          // third part code can now interact with 
          // `this.domNode`.
        }

        componentWillUnmount() {
          // If the third party code has a destructor,
          // use it here.
        }
    
        render() {
          const { children } = this.props;
        
          return (
            <div
              ref={domNode => this.domNode = domNode}
            >{children}</div>
          );
        }
    }

